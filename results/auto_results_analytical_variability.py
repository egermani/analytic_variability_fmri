import warnings

# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)
simplefilter(action='ignore', category=UserWarning)
simplefilter(action='ignore', category=RuntimeWarning)

from os.path import join as opj
import os
import urllib.request
from zipfile import ZipFile
import shutil
from glob import glob
import sys
import getopt

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from lib import (download_reproduced_maps, preprocess_maps, acceptance_plot, 
                 view_maps, dice, mse, correlation, activated_voxels)
from nilearn import image, datasets, plotting


if __name__ == "__main__":
	teams = []
	repro_dir = None
	fig_dir = None
	invert = False

	# Options 
	try:
		OPTIONS, REMAINDER = getopt.getopt(sys.argv[1:], 'r:t:f:i', ['repro_dir=', 'team_ID=', 'figures_dir=', 'invert'])

	except getopt.GetoptError as err:
		print(err)
		sys.exit(2)

	# Replace variables depending on options
	for opt, arg in OPTIONS:
		if opt in ('-r', '--repro_dir'):
			repro_dir = arg
		elif opt in ('-t', '--team_ID'):
			teams.append(arg)
		elif opt in ('-f', '--figures_dir'): 
			fig_dir = arg
		elif opt in ('-i', '--invert'): 
			invert = True


	print('OPTIONS   :', OPTIONS)

	exp_dir = opj(repro_dir, 'NARPS-reproduction')
	result_dir = opj(fig_dir, 'analytical_variability')

	download_reproduced_maps.get_all_reproduced_maps(repro_dir)

	if not os.path.isdir(result_dir):
		os.mkdir(result_dir)

	n_val = [20, 40, 60, 80, 108]

	repro_unthresh= {}
	repro_thresh = {}

	for team_ID in teams:
		repro_unthresh.update({team_ID : {}})
		repro_thresh.update({team_ID : {}})
		if team_ID == 'X19V':
			for i, n in enumerate(n_val):

				repro_unthresh[team_ID].update({str(n) : opj(exp_dir, 
                                                                 f"team_{team_ID}_nsub_{n}_hypo5_unthresholded.nii.gz")})

				repro_thresh[team_ID].update({str(n) : opj(exp_dir,                                        
                                                               f"team_{team_ID}_nsub_{n}_hypo5_thresholded.nii.gz")})
		else:
			for i, n in enumerate(n_val):

				repro_unthresh[team_ID].update({str(n) : opj(exp_dir, 
                                                                 f"team_{team_ID}_nsub_{n}_hypo5_unthresholded.nii")})

				repro_thresh[team_ID].update({str(n) : opj(exp_dir,                                        
                                                               f"team_{team_ID}_nsub_{n}_hypo5_thresholded.nii")})

	hypothesis = {'5':'Negative effect in of loss VMPFC - for the equal indifference group'}

	if 'X19V' in teams:
		for n in n_val:
			inv_file = opj(exp_dir, f"inv_team_X19V_nsub_{n}_hypo5_unthresholded.nii")
			if not os.path.isfile(inv_file):
				img = preprocess_maps.get_inv_data(repro_unthresh['X19V'][str(n)], inv_file)
			repro_unthresh['X19V'][str(n)] = opj(exp_dir, f"inv_team_X19V_nsub_{n}_hypo5_unthresholded.nii")

	for team_ID in teams:
		if team_ID in ['2T6S', 'C88N', 'Q6O0', 'V55J', 'J7F9']:
			for n in [20, 40, 60, 80, 108]:
				t_stat_file = repro_unthresh[team_ID][str(n)]
				z_stat_file = opj(exp_dir, f"zstat_team_{team_ID}_nsub_{n}_hypo5_unthresholded.nii")
				N = n/2
				if not os.path.exists(z_stat_file):
					preprocess_maps.t_to_z(t_stat_file, z_stat_file, N)
				repro_unthresh[team_ID][str(n)] = z_stat_file
                
				t_stat_file = repro_thresh[team_ID][str(n)]
				z_stat_file = opj(exp_dir, f"zstat_team_{team_ID}_nsub_{n}_hypo5_thresholded.nii")
				N = n/2
				if not os.path.exists(z_stat_file):
					preprocess_maps.t_to_z(t_stat_file, z_stat_file, N)
				preprocess_maps.t_to_z(t_stat_file, z_stat_file, N)
				repro_thresh[team_ID][str(n)] = z_stat_file

	atlas_cort = datasets.fetch_atlas_harvard_oxford('cort-prob-2mm')

	coords = plotting.find_probabilistic_atlas_cut_coords(atlas_cort.maps)

	vmPFC = (int(coords[24][0]), int(coords[24][1]), int(coords[24][2]))
    
	activated_voxels.plot_stat_value_from_roi(teams, n_val, repro_thresh, repro_unthresh,24, atlas_cort, result_dir)
    
	view_maps.plot_statmap_unthresh_thresh_analytical_variability(repro_thresh, repro_unthresh, n_val, teams, result_dir, coords_cut=vmPFC, atlas=atlas_cort, lab=24)
        
	view_maps.plot_statmap_unthresh_gb_analytical_variability(repro_thresh, repro_unthresh, n_val, teams, result_dir, coords_cut=vmPFC, atlas=atlas_cort, lab=24)

	view_maps.plot_unthresholded_statmap_analytical_variability(repro_unthresh, n_val, 
                                                                teams, result_dir, coords_cut = vmPFC)

	view_maps.plot_statmap_rois_analytical_variability(repro_thresh, n_val, teams, result_dir, 
                                                       coords_cut = vmPFC, atlas = atlas_cort, lab = 24)
	view_maps.plot_statmap_gb_analytical_variability(repro_thresh, n_val, teams, result_dir, 
                                                       coords_cut = vmPFC, atlas = atlas_cort, lab = 24)

	acceptance_plot.plot_acceptance_proportion(teams, n_val, result_dir)

	dice.plot_dice_analytical_variability(n_val, repro_thresh, teams, result_dir)

	mse.plot_mse_analytical_variability(teams, repro_unthresh, n_val, result_dir)

	correlation.plot_correlation_analytical_variability(teams, n_val, repro_unthresh, result_dir)

	activated_voxels.plot_stat_value_from_roi(teams, n_val, repro_thresh, repro_unthresh,24, atlas_cort, result_dir)





















