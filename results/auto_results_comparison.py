import warnings

# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)
simplefilter(action='ignore', category=UserWarning)
simplefilter(action='ignore', category=RuntimeWarning)


from lib import view_maps, activated_voxels, bland_altman, correlation, dice, download_original_maps, download_reproduced_maps, euler_chars, mse
import pandas as pd 
import os 
from os.path import join as opj
import sys
import getopt
import json
from nilearn import datasets, plotting
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np

"""
python auto_results_comparison.py -o /local/egermani/analytic_variability_fmri/data/derived/original -r /local/egermani/analytic_variability_fmri/data/derived/reproduced -f /local/egermani/analytic_variability_fmri/figures -t V55J
"""

if __name__ == "__main__":
	team_list = []
	repro_dir = None
	fig_dir = None
	original_dir = None

	# Options 
	try:
	    OPTIONS, REMAINDER = getopt.getopt(sys.argv[1:], 'o:r:t:f:', ['original_dir=', 'repro_dir=', 'team_ID=', 'figures_dir='])

	except getopt.GetoptError as err:
	    print(err)
	    sys.exit(2)

	# Replace variables depending on options
	for opt, arg in OPTIONS:
	    if opt in ('-r', '--repro_dir'):
	        repro_dir = arg
	    elif opt in ('-t', '--team_ID'):
	        team_list.append(arg)
	    elif opt in ('-f', '--figures_dir'): 
	        fig_dir = arg
	    elif opt in ('-o', '--original_dir'): 
	        original_dir = arg


	print('OPTIONS   :', OPTIONS)

	# List of hypotheses
	hypothesis = ['Positive effect of gain in ventromedial PFC - for the equal indifference group',
	'Positive effect in of gain ventromedial PFC - for the equal range group', 
	'Positive effect in of gain ventral striatum - for the equal indifference group',
	'Positive effect in of gain ventral striatum - for the equal range group',
	'Negative effect in of loss VMPFC - for the equal indifference group',
	'Negative effect in of loss VMPFC - for the equal range group',
	'Positive effect in of loss amygdala - for the equal indifference group',
	'Positive effect in of loss amygdala - for the equal range group',
	'Greater positive response to losses in amygdala for equal range condition vs. equal indifference condition.']

	for team_ID in team_list:
		print(f"Computing results comparison for team {team_ID}")
		# Creation of the figures directory
		if not os.path.isdir(fig_dir):
			os.mkdir(fig_dir) # Creation of quality measures directory
		figures_dir = opj(fig_dir, team_ID) # Directory specific to the team ID
		if not os.path.isdir(figures_dir): # Creation of the specific directory if not exists
			os.mkdir(figures_dir)

		# List of files 
		n_sub = 108
		repro_unthresh = [opj(repro_dir, "NARPS-reproduction",
	                      f"team_{team_ID}_nsub_{n_sub}_hypo{i}_unthresholded.nii") for i in range(1,10)]

		repro_thresh = [opj(repro_dir, "NARPS-reproduction",
	                      f"team_{team_ID}_nsub_{n_sub}_hypo{i}_thresholded.nii") for i in range(1,10)]

		# Coordinates for ROI 
		atlas_cort = datasets.fetch_atlas_harvard_oxford('cort-prob-2mm')
		atlas_sub = datasets.fetch_atlas_harvard_oxford('sub-prob-2mm')

		coords = plotting.find_probabilistic_atlas_cut_coords(atlas_cort.maps)
		coords_sub = plotting.find_probabilistic_atlas_cut_coords(atlas_sub.maps)

		vmPFC = (int(coords[24][0]), int(coords[24][1]), int(coords[24][2]))
		vs = (int(coords_sub[19][0]), int(coords_sub[19][1]), int(coords_sub[19][2]))
		amygdala = (int(coords_sub[20][0]), int(coords_sub[20][1]), int(coords_sub[20][2]))

		# Specific informations of the team
		if team_ID == '2T6S':
			collection = '4881'

			if not os.path.isdir(opj(original_dir, f"NARPS-{team_ID}")):
				download_original_maps.get_original_maps(collection, team_ID, original_dir)

			# Specific coordinates used by the team
			vmPFC = (2, 46, -8)
			vs = (-12, 12, -6)
			amygdala = (24, -4, -18)

			# Specific names 
			original_unthresh = [opj(original_dir, f"NARPS-{team_ID}", 'hypo1_unthresholded.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo2_unthresholded.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo3_unthresholded.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo4_unthresholded.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo7_unthresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo6_unthresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo5_unthresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo8_unthresholded_revised_1.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo9_unthresholded.nii.gz')]

			original_thresh = [opj(original_dir, f"NARPS-{team_ID}", 'hypo1_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo2_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo3_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo4_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo5_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo6_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo7_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo8_thresholded_revised.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo9_thresholded_revised.nii.gz')]


		if team_ID == 'C88N':
			collection = '4812'

			if not os.path.isdir(opj(original_dir, f"NARPS-{team_ID}")):
				download_original_maps.get_original_maps(collection, team_ID, original_dir)

			original_unthresh = [opj(original_dir, f"NARPS-{team_ID}", 'hypo1_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo2_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo3_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo4_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo5_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo6_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo7_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo8_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo9_unthresh.nii.gz')]

			original_thresh = [opj(original_dir, f"NARPS-{team_ID}", 'hypo1_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo2_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo3_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo4_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo5_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo6_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo7_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo8_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo9_thresh.nii.gz')]

		if team_ID == 'Q6O0':
			collection = '4968'

			if not os.path.isdir(opj(original_dir, f"NARPS-{team_ID}")):
				download_original_maps.get_original_maps(collection, team_ID, original_dir)

			original_unthresh = [opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001_1.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001_2.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001_3.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0002.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0002_1.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001_4.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001_5.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'spmT_0001_6.nii.gz')]

			original_thresh = [opj(original_dir, f"NARPS-{team_ID}", 'gain_positive.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'gain_positive_1.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'gain_positive_2.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'gain_positive_3.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'loss_negative.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'loss_negative_1.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'loss_positive.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'loss_positive_1.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'loss_positive_difference.nii.gz')]
            
		if team_ID == "V55J":
			collection = '4919'
            
			if not os.path.isdir(opj(original_dir, f"NARPS-{team_ID}")):
				download_original_maps.get_original_maps(collection, team_ID, original_dir)

			original_unthresh = [opj(original_dir, f"NARPS-{team_ID}", 'hypo1_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo2_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo3_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo4_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo5_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo6_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo7_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo8_unthresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo9_unthresh.nii.gz')]

			original_thresh = [opj(original_dir, f"NARPS-{team_ID}", 'hypo1_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo2_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo3_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo4_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo5_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo6_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo7_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo8_thresh.nii.gz'),
			    opj(original_dir, f"NARPS-{team_ID}", 'hypo9_thresh.nii.gz')]

		# Plot images
		view_maps.plot_unthresholded_glassbrain_comparisons(original_unthresh, repro_unthresh, hypothesis, figures_dir, team_ID)
		view_maps.plot_glassbrain_comparisons(original_thresh, repro_thresh, hypothesis, figures_dir, team_ID)
		view_maps.plot_statmap_comparisons(original_thresh, repro_thresh, hypothesis, figures_dir, team_ID, vmPFC, vs, amygdala)
		view_maps.plot_statmap_rois_comparisons(original_thresh, repro_thresh, hypothesis, figures_dir, team_ID, vmPFC, vs, amygdala)
		view_maps.plot_unthresholded_statmap_comparisons(original_unthresh, repro_unthresh, hypothesis, figures_dir, team_ID, vmPFC, vs, amygdala)

		# Correlation coefficient 
		coeff_list = []

		print(f"Pearson\'s product-moment correlation coefficients between reproduced and original maps for team {team_ID}")

		for i in range(len(repro_unthresh)):
		    coeff_list.append(correlation.get_corr_coeff(repro_unthresh[i], original_unthresh[i], method = 'pearson'))
		    print(hypothesis[i], ':', coeff_list[i])
		    
		coeff_df = pd.Series(coeff_list)
		coeff_df.to_csv(opj(figures_dir, f"corr_coeff_{team_ID}.csv"))

		# MSE
		print(f"Mean Squared Errors between UNTHRESHOLDED reproduced and original maps for team {team_ID}")

		mse_list = []

		for i in range(len(original_unthresh)):
			mse_list.append(mse.get_mse(original_unthresh[i], repro_unthresh[i]))
			print(hypothesis[i], ':', mse_list[i])
		    
		mse_df = pd.Series(mse_list)
		mse_df.to_csv(opj(figures_dir, f"mse_{team_ID}.csv"))

		# Bland-Altman plots
		plt.style.use('seaborn-colorblind')

		f = plt.figure(figsize=(10, 60))

		gs0 = gridspec.GridSpec(len(repro_unthresh), 1, f)

		x_label = ' of T-statistics'
		y_label = ' of T-statistics'
		lims=(-10,10,-8,8)

		for i in range(len(repro_unthresh)):
		    gs00 = gridspec.GridSpecFromSubplotSpec(
		        5, 6, subplot_spec=gs0[i], hspace=0.50, wspace=1.3)

		    bland_altman.bland_altman_plot(f, gs00, repro_unthresh[i], original_unthresh[i],
		                  hypothesis[i],
		                  x_label,
		                  y_label,
		                  False,
		                  lims=lims)

		plt.savefig(opj(figures_dir, f"bland_altman_{team_ID}.png"))

		# Euler characteristic curves
		thresholds = np.arange(-6, 6, 0.2)

		f = plt.figure(figsize=(7, 25))
		gs = gridspec.GridSpec(len(original_unthresh), 1, f, hspace = 0.5)

		plt.style.use('default')

		for i in range(len(original_unthresh)):
		    euler_chars.plot_euler_chars(f, gs, i, original_unthresh[i], repro_unthresh[i], thresholds, hypothesis[i])

		plt.savefig(opj(figures_dir, f"euler_chars_{team_ID}.png"))

		# Activated voxels in each maps 

		activated_voxels_list = []

		for i in range(len(original_thresh)):
			activated_voxels_list.append((activated_voxels.get_number_activated_voxels(original_thresh[i]), activated_voxels.get_number_activated_voxels(repro_thresh[i])))
			print(hypothesis[i], ': \n', 'Original:', activated_voxels_list[i][0], ', Reproduction: ', activated_voxels_list[i][1])
		    
		activated_voxels_df = pd.Series(activated_voxels_list)
		activated_voxels_df.to_csv(opj(figures_dir, f"activated_voxels_{team_ID}.csv"))

		# Non overlapping voxels 
		print('Number of non-overlapping voxels between reproduced thresholded and original maps')

		non_overlapping_voxels_list = []

		for i in range(len(original_thresh)):
			non_overlapping_voxels_list.append(activated_voxels.get_overlap_bw_voxels(original_thresh[i], repro_thresh[i], 
		                                                opj(figures_dir, f"overlap_hypo{i+1}_{team_ID}.nii"), 
		                                                opj(figures_dir, f"non_overlap_hypo{i+1}_{team_ID}.nii"))[1])
			print(hypothesis[i], ':', non_overlapping_voxels_list[i])

		non_overlap_df = pd.Series(non_overlapping_voxels_list)
		non_overlap_df.to_csv(opj(figures_dir, f"non_overlap_{team_ID}.csv"))

		print('DICE coefficient between reproduced thresholded maps and original ones')

		dice_list = []

		for i in range(len(original_thresh)):
			dice_list.append(dice.get_dice(original_thresh[i], repro_thresh[i]))
			print(hypothesis[i], ':', dice_list[i])

		dice_df = pd.Series(dice_list)
		dice_df.to_csv(opj(figures_dir, f"dice_{team_ID}.csv"))





	





    
