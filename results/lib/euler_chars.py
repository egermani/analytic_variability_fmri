import numpy as np
from skimage.measure import euler_number
import nibabel as nib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os

def get_euler_chars(original, reproduced, thresholds):
    euler_chars_orig = []
    euler_chars_repro = []
    
    orig = nib.load(original)
    repro = nib.load(reproduced)
    
    orig_data = orig.get_fdata()
    repro_data = repro.get_fdata()
    
    for t in thresholds: 
        thresh_orig_data = orig_data >= t
        thresh_repro_data = repro_data >= t
        
        thresh_orig_data = thresh_orig_data.astype(int)
        thresh_repro_data = thresh_repro_data.astype(int)
        
        euler_chars_orig.append(euler_number(thresh_orig_data))
        euler_chars_repro.append(euler_number(thresh_repro_data))
    
    return euler_chars_orig, euler_chars_repro



def plot_euler_chars(f, gs, i, original_map, reproduced_map, thresholds, hypothesis):

	euler_chars_orig, euler_chars_repro = get_euler_chars(original_map, reproduced_map, thresholds)
	ec_plot = f.add_subplot(gs[i])
	ec_plot.plot(thresholds, euler_chars_orig, lw = '1.5', label = 'Original')
	ec_plot.plot(thresholds, euler_chars_repro, lw = '1.5', label = 'Reproduced')
	ec_plot.set_xlabel('Threshold')
	ec_plot.set_ylabel('Euler characteristic')
	ec_plot.legend(['Original', 'Reproduced'])
	ec_plot.set_title(hypothesis)
