import nibabel as nib
import numpy as np
from nibabel.processing import resample_from_to
from nilearn import datasets, image, masking
import matplotlib.pyplot as plt 
import matplotlib.gridspec as gridspec
from os.path import join as opj

def get_data(img_file):
	img = nib.load(img_file)
	img_data = img.get_fdata()
	img_data = np.nan_to_num(img_data)

	return img_data


def get_number_activated_voxels(img_file):
	img_data = get_data(img_file)
	img_data_act = img_data > 0

	n_activated_voxels = img_data_act.sum()

	return n_activated_voxels

def get_overlap_bw_voxels(img_file1, img_file2, filename_overlap, filename_non_overlap):
	img_data1 = get_data(img_file1)
	img_data2 = get_data(img_file2)

	img_data1_act = img_data1 > 0 
	img_data2_act = img_data2 > 0

	overlap_map = img_data1_act.astype(int) + img_data2_act.astype(int)

	non_overlap_map = overlap_map == 1
	overlap_map = overlap_map > 1
	overlap_map = overlap_map.astype(int)
	non_overlap_map = non_overlap_map.astype(int)

	img_non_overlap = nib.Nifti1Image(non_overlap_map, None)
	nib.save(img_non_overlap, filename_non_overlap)

	img_overlap = nib.Nifti1Image(overlap_map, None)
	nib.save(img_overlap, filename_overlap)

	n_overlap_voxels = overlap_map.sum()
	n_non_overlap_voxels = non_overlap_map.sum()

	return n_overlap_voxels, n_non_overlap_voxels, filename_non_overlap, filename_overlap


def get_stat_value_from_roi(atlas, lab, thresh_map):
    mask = masking.compute_brain_mask(image.index_img(atlas.maps, lab))
    img = nib.load(thresh_map)
    mask_resampled = resample_from_to(mask, img, order=0)
    
    masked_data = np.nan_to_num(img.get_fdata()) * mask_resampled.get_fdata()
    
    mean_stat = np.mean(masked_data[masked_data != 0])
    
    if mean_stat is None or np.isnan(mean_stat):
        mean_stat = 0 
    
    max_stat = np.amax(masked_data)
    min_stat = np.amin(masked_data)
    
    return mean_stat, min_stat, max_stat

def get_stat_dict(teams, n_val, repro_thresh, lab, atlas):
	mean_value_dict = {}
	min_value_dict = {}
	max_value_dict = {}
	for team_ID in teams:
	    mean_value_dict.update({team_ID : {}})
	    min_value_dict.update({team_ID : {}})
	    max_value_dict.update({team_ID : {}})
	    for i_n, n in enumerate(n_val): 
	        mean_value_dict[team_ID].update({str(n) : {}})
	        min_value_dict[team_ID].update({str(n) : {}})
	        max_value_dict[team_ID].update({str(n) : {}})
	        mean_value_dict[team_ID][str(n)], min_value_dict[team_ID][str(n)], max_value_dict[team_ID][str(n)] = get_stat_value_from_roi(atlas, lab, repro_thresh[team_ID][str(n)])

	return mean_value_dict, min_value_dict, max_value_dict

def plot_stat_value_from_roi(teams, n_val, repro_thresh, repro_unthresh, lab, atlas, result_dir):
	mean_value_dict, min_value_dict, max_value_dict = get_stat_dict(teams, n_val, repro_thresh, lab, atlas)
	mean_value_dict_unthresh, min_value_dict_unthresh, max_value_dict_unthresh = get_stat_dict(teams, n_val, repro_unthresh, lab, atlas)

	f = plt.figure(figsize = (10, 7))
	plt.grid()
	f.suptitle(f"Mean statistical value inside vmPFC \n for H5 for all teams depending on sample size", fontweight="bold")

	for team_ID in teams:
	    plt.scatter(n_val, mean_value_dict[team_ID].values())
	    plt.plot(n_val, mean_value_dict[team_ID].values(), label = team_ID, ls = 'dotted', lw = 3)
	    plt.xlabel('Number of participants')
	    plt.ylabel('Mean statistical value')

	f.legend(fontsize=12)
	f.savefig(opj(result_dir, "mean_value_inside_ROI.png"))
	plt.close()


	#f = plt.figure()
	plt.grid()
	#f.suptitle(f"Max stat value inside ROI for hypothesis 5 for all teams depending on sample size")
    
	for team_ID in teams:
	    plt.scatter(n_val, max_value_dict[team_ID].values())
	    plt.plot(n_val, max_value_dict[team_ID].values(), label = team_ID)

	plt.legend(loc='lower right', fontsize=12)
	plt.savefig(opj(result_dir, "max_value_inside_ROI.png"))  
	plt.close()
    
	#f = plt.figure()
	plt.grid()
	#f.suptitle(f"Max stat value inside ROI for hypothesis 5 for all teams depending on sample size")
    
	for team_ID in teams:
	    plt.scatter(n_val, max_value_dict_unthresh[team_ID].values())
	    plt.plot(n_val, max_value_dict_unthresh[team_ID].values(), label = team_ID)

	plt.legend(loc='lower right', fontsize=12)
	plt.savefig(opj(result_dir, "max_value_inside_ROI_unthresh.png"))  
	plt.close()
    
	ratio_per_sample_size = []
	for n in n_val:
		val_list = [max_value_dict_unthresh[t][str(n)] for t in teams]
		if np.min(val_list) == 0:
			ratio_per_sample_size.append(np.max(val_list))
		else:     
			ratio_per_sample_size.append(np.max(val_list)/np.min(val_list))
          
	f = plt.figure()
	plt.grid()
	#f.suptitle(f"Ratios of max/min in all teams max stat value inside ROI for hypothesis 5 depending on sample size")
	plt.scatter(n_val, ratio_per_sample_size)
	plt.plot(n_val, ratio_per_sample_size, ls = 'dotted', lw = 3)
	f.savefig(opj(result_dir, "ratio_max_value_inside_ROI.png")) 
    

        