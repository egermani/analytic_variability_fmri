import nibabel as nib
import numpy as np
from nibabel.processing import resample_from_to
import matplotlib.pyplot as plt 
import matplotlib.gridspec as gridspec
from os.path import join as opj


def mask_using_nan(data_img):
    # Set masking using NaN's
    data_orig = data_img.get_fdata()

    if np.any(np.isnan(data_orig)):
        # Already using NaN
        data_img_nan = data_img
    else:
        # Replace zeros by NaNs
        data_nan = data_orig
        data_nan[data_nan == 0] = np.nan
        # Save as image
        data_img_nan = nib.Nifti1Image(data_nan, data_img.affine)

    return(data_img_nan)


def get_masked_vectorised_data(data1_file, data2_file, reslice_on_2 = True):
    # Load nifti images
    data1_img = nib.load(data1_file)
    data2_img = nib.load(data2_file)

    # Set masking using NaN's
    data1_img = mask_using_nan(data1_img)
    data2_img = mask_using_nan(data2_img)

    if reslice_on_2:
        # Resample data1 on data2 using nearest nneighbours
        data1_resl_img = resample_from_to(data1_img, data2_img, order=0)

        # Load data from images
        data1 = data1_resl_img.get_fdata()
        data2 = data2_img.get_fdata()
    else:
        # Resample data2 on data1 using nearest nneighbours
        data2_resl_img = resample_from_to(data2_img, data1_img, order=0)

        # Load data from images
        data1 = data1_img.get_fdata()
        data2 = data2_resl_img.get_fdata()

    # Vectorise input data
    data1 = np.reshape(data1, -1)
    data2 = np.reshape(data2, -1)


    in_mask_indices = np.logical_not(
        np.logical_or(
            np.logical_or(np.isnan(data1), np.absolute(data1) == 0),
            np.logical_or(np.isnan(data2), np.absolute(data2) == 0)))

    data1 = data1[in_mask_indices]
    data2 = data2[in_mask_indices]

    return data1, data2

def get_mse(data1_file, data2_file):
    '''
    Return the Mean Square Error between 2 images. 

    Parameters : 
    - img_file1 & img_file2 : images to compare in nii format.

    Return : 
    - mse : Mean Square Error between 2 images.
    '''
    data1, data2 = get_masked_vectorised_data(data1_file, data2_file, reslice_on_2 = True)

    if len(data1) == 0 or len(data2) == 0:
        print("Images with 0 common activated voxels given. No metric calculable. ")
        return 0

    diff_array = data1 - data2

    sqr_diff_array = np.square(diff_array)

    mse = np.mean(sqr_diff_array)

    return mse

def get_sse(data1_file, data2_file):
    '''
    Return the Sum Square Error between 2 images. 

    Parameters : 
    - img_file1 & img_file2 : images to compare in nii format.

    Return : 
    - sse : Sum Square Error between 2 images.
    '''
    data1, data2 = get_masked_vectorised_data(data1_file, data2_file, reslice_on_2 = True)

    if len(data1) == 0 or len(data2) == 0:
        return f"Images with 0 common activated voxels given. No metric calculable."

    diff_array = data1 - data2

    sqr_diff_array = np.square(diff_array)

    sse = np.sum(sqr_diff_array)

    return sse  


def get_pairwise_mse(teams, repro_unthresh, n_val):
    mse_dict = {}

    for i in range(len(teams)-1):
        for j in range(i, len(teams)):
            if i != j:
                mse_dict.update({f"{teams[i]}-{teams[j]}" : []})
                for n in n_val:
                    mse_val = get_mse(repro_unthresh[teams[i]][str(n)], repro_unthresh[teams[j]][str(n)])
                    mse_dict[f"{teams[i]}-{teams[j]}"].append(mse_val)

    return mse_dict

def get_mean_pairwise_mse(n_val, mse_dict):
    mean_mse_list = []

    for i, n in enumerate(n_val):
        mean_mse_list.append(np.mean(np.array(list(mse_dict.values()))[:,i]))

    return mean_mse_list


def plot_mse_analytical_variability(teams, repro_unthresh, n_val, result_dir):
    mse_dict = get_pairwise_mse(teams, repro_unthresh, n_val)

    mean_mse_list = get_mean_pairwise_mse(n_val, mse_dict)

    f = plt.figure(figsize = (10, 7))
    plt.title(f"Pairwise MSE between unthresholded maps \n for Hypotheses 5 depending on sample sizes.")
    plt.grid()
    for team_comp in mse_dict.keys():
        plt.scatter(n_val, mse_dict[team_comp])
        plt.plot(n_val, mse_dict[team_comp], label = team_comp, ls = 'dotted')
    plt.scatter(n_val, mean_mse_list,color = 'red')
    plt.plot(n_val, mean_mse_list, label = 'Mean pairwise MSE', color = 'red', lw = 3)
    f.legend()
    f.savefig(opj(result_dir, f"h5_mse.png"))



