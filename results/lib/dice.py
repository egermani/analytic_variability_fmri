import nibabel as nib
import numpy as np
from lib import activated_voxels
from scipy.spatial import distance
from nibabel.processing import resample_from_to
import matplotlib.pyplot as plt 
import matplotlib.gridspec as gridspec
from os.path import join as opj

def get_dice(img_file1, img_file2, reslice_on_2 = True):
    img1 = nib.load(img_file1)
    img2 = nib.load(img_file2)
    
    if reslice_on_2:
        # Resample data1 on data2 using nearest nneighbours
        img1_resl = resample_from_to(img1, img2, order=0)

        # Load data from images
        data1 = img1_resl.get_fdata()
        data2 = img2.get_fdata()
        
    else:
        # Resample data2 on data1 using nearest nneighbours
        img2_resl = resample_from_to(img2, img1, order=0)

        # Load data from images
        data1 = img1.get_fdata()
        data2 = img2_resl.get_fdata()
    
    img_data1 = np.nan_to_num(data1)
    img_data2 = np.nan_to_num(data2)

    img_data1_act = img_data1 > 0 
    img_data2_act  = img_data2 > 0

    overlap_map = img_data1_act.astype(int) + img_data2_act.astype(int)

    overlap_map = overlap_map > 1
    overlap_map = overlap_map.astype(int)

    n_overlap_voxels = overlap_map.sum()

    n_act_voxels_1 = activated_voxels.get_number_activated_voxels(img_file1)
    n_act_voxels_2 = activated_voxels.get_number_activated_voxels(img_file2)

    if n_act_voxels_1 == 0 or n_act_voxels_2 == 0:
        dice = 0
    else:
        dice = 2 * n_overlap_voxels / (n_act_voxels_1 + n_act_voxels_2)

    return dice

def get_pairwise_dice(teams, repro_thresh, n_val):
    dice_dict = {}

    for i in range(len(teams)-1):
        for j in range(i, len(teams)):
            if i != j:
                dice_dict.update({f"{teams[i]}-{teams[j]}" : []})
                for n in n_val:
                    dice_val = get_dice(repro_thresh[teams[i]][str(n)], repro_thresh[teams[j]][str(n)])
                    dice_dict[f"{teams[i]}-{teams[j]}"].append(dice_val)

    return dice_dict

def get_mean_pairwise_dice(n_val, dice_dict):
    mean_dice_list = []

    for i, n in enumerate(n_val):
        mean_dice_list.append(np.mean(np.array(list(dice_dict.values()))[:,i]))

    return mean_dice_list

def plot_dice_analytical_variability(n_val, repro_thresh, teams, result_dir):
    dice_dict = get_pairwise_dice(teams, repro_thresh, n_val)

    mean_dice_list = get_mean_pairwise_dice(n_val, dice_dict)

    f = plt.figure(figsize = (10, 7))
    plt.title(f"Pairwise dice coefficient between thresholded maps \n for Hypothesis 5 depending on sample sizes.")
    plt.grid()
    for team_comp in dice_dict.keys():
        plt.scatter(n_val, dice_dict[team_comp])
        plt.plot(n_val, dice_dict[team_comp], ls = 'dotted', label = team_comp)
    plt.scatter(n_val, mean_dice_list,color = 'red')
    plt.plot(n_val, mean_dice_list, label = 'Mean pairwise Dice', color = 'red', lw = 3)

    f.legend()
    f.savefig(opj(result_dir, f"h5_dice.png"))

    


