import nibabel as nib 
import numpy as np 
from scipy import stats

def get_inv_data(img_file, inv_img_file):
	img = nib.load(img_file)
	img_data = img.get_fdata()
	img_data = np.nan_to_num(img_data)
	img_data = img_data * -1

	img_inv = nib.Nifti1Image(img_data, img.affine)

	nib.save(img_inv, inv_img_file)

	return img_inv


def t_to_z(t_stat_file, z_stat_file, N):
    # Convert t-statistic images to z-statistic images used for the consensus analysis
    df = N-1

    t_stat_img = nib.load(t_stat_file)

    t_stat = t_stat_img.get_data()
    z_stat = np.zeros_like(t_stat)

    # Handle large and small values differently to avoid underflow
    z_stat[t_stat < 0] = -stats.norm.ppf(stats.t.cdf(-t_stat[t_stat < 0], df))
    z_stat[t_stat > 0] = stats.norm.ppf(stats.t.cdf(t_stat[t_stat > 0], df))

    z_stat_img = nib.Nifti1Image(z_stat, t_stat_img.affine)
    nib.save(z_stat_img, z_stat_file)
    return(z_stat_file)

def pval_to_z(pval_file, z_stat_file):
    pval_img = nib.load(pval_file)

    pval = pval_img.get_data()
    z_stat = np.zeros_like(pval)

    z_stat[pval > 0] = stats.norm.ppf(pval[pval > 0])

    z_stat_img = nib.Nifti1Image(z_stat, pval_img.affine)
    nib.save(z_stat_img, z_stat_file)
    return(z_stat_file)