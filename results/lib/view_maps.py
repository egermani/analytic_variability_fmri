from os.path import join as opj
from nilearn import plotting, image, datasets
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from nilearn.plotting.cm import _cmap_d as nilearn_cmaps

def plot_glassbrain_comparisons(original_thresh, repro_thresh, hypothesis, figures_dir, team_ID):
	for i in range(len(original_thresh)):
	    f = plt.figure(figsize = (15, 7))
	    gs = f.add_gridspec(2, 1)
	    ax1 = f.add_subplot(gs[0, 0])
	    ax2 = f.add_subplot(gs[1, 0])
	    f.suptitle(f"Thresholded statistical maps for Hypothesis {i+1} \n {hypothesis[i]}",
	               fontweight = 'bold')
	    plotting.plot_glass_brain(original_thresh[i], title = f"Original by Team {team_ID}", 
	                        figure = f, axes = ax1, colorbar = True)
	    plotting.plot_glass_brain(repro_thresh[i], title = f"Reproduction of Team {team_ID} pipeline",
	                        figure = f, axes = ax2, colorbar = True)
	    f.savefig(opj(figures_dir, f"thresholded_glassbrain_hypo{i+1}_{team_ID}.png"))

def plot_unthresholded_glassbrain_comparisons(original_unthresh, repro_unthresh, hypothesis, figures_dir, team_ID):
	for i in range(len(original_unthresh)):
	    f = plt.figure(figsize = (15, 7))
	    gs = f.add_gridspec(2, 1)
	    ax1 = f.add_subplot(gs[0, 0])
	    ax2 = f.add_subplot(gs[1, 0])
	    f.suptitle(f"Thresholded statistical maps for Hypothesis {i+1} \n {hypothesis[i]}",
	               fontweight = 'bold')
	    plotting.plot_glass_brain(original_unthresh[i], title = f"Original by Team {team_ID}", 
	                        figure = f, axes = ax1, colorbar = True, plot_abs=False, cmap=nilearn_cmaps['cold_hot'], vmin=-10,
                                 vmax=10)
	    plotting.plot_glass_brain(repro_unthresh[i], title = f"Reproduction of Team {team_ID} pipeline",
	                        figure = f, axes = ax2, colorbar = True, plot_abs=False, cmap=nilearn_cmaps['cold_hot'], vmin=-10, 
                                 vmax=10)
	    f.savefig(opj(figures_dir, f"unthresholded_glassbrain_hypo{i+1}_{team_ID}.png"))

def plot_statmap_comparisons(original_thresh, repro_thresh, hypothesis, figures_dir, team_ID, vmPFC, vs, amygdala):
	for i in range(len(original_thresh)):
	    if i in [0,1,4,5]:
	        coords_cut = vmPFC
	    elif i in [2,3]:
	        coords_cut = vs
	    else:
	        coords_cut = amygdala
	        
	    f = plt.figure(figsize = (15, 7))
	    gs = f.add_gridspec(2, 1)
	    ax1 = f.add_subplot(gs[0, 0])
	    ax2 = f.add_subplot(gs[1, 0])
	    f.suptitle(f"Thresholded statistical maps for Hypothesis {i+1} \n {hypothesis[i]}",
	               fontweight = 'bold')
	    plotting.plot_stat_map(original_thresh[i], title = f"Original by Team {team_ID}", 
	                        cut_coords = coords_cut, figure = f, axes = ax1)
	    plotting.plot_stat_map(repro_thresh[i], title = f"Reproduction of Team {team_ID} pipeline",
	                        cut_coords = coords_cut, figure = f, axes = ax2)
	    f.savefig(opj(figures_dir, f"thresholded_spm_hypo{i+1}_{team_ID}.png"))



def plot_statmap_rois_comparisons(original_thresh, repro_thresh, hypothesis, figures_dir, team_ID, vmPFC, vs, amygdala):
	atlas_cort = datasets.fetch_atlas_harvard_oxford('cort-prob-2mm')
	atlas_sub = datasets.fetch_atlas_harvard_oxford('sub-prob-2mm')
	
	for i in range(len(original_thresh)):
	    if i in [0,1,4,5]:
	        coords_cut = vmPFC
	    elif i in [2,3]:
	        coords_cut = vs
	    else:
	        coords_cut = amygdala
	        
	    f = plt.figure(figsize = (15, 7))
	    gs = f.add_gridspec(2, 1)
	    ax1 = f.add_subplot(gs[0, 0])
	    ax2 = f.add_subplot(gs[1, 0])
	    f.suptitle(f"Thresholded statistical maps for Hypothesis {i+1} \n {hypothesis[i]}",
	               fontweight = 'bold')
	    display_orig = plotting.plot_stat_map(original_thresh[i], title = f"Original by Team {team_ID}", 
	                        cut_coords = coords_cut, figure = f, axes = ax1)
	    display_repro = plotting.plot_stat_map(repro_thresh[i], title = f"Reproduction of Team {team_ID} pipeline",
	                        cut_coords = coords_cut, figure = f, axes = ax2)
	    
	    if i in [0,1,4,5]:
	        atlas = atlas_cort
	        lab = 24
	        display_orig.add_contours(image.index_img(atlas.maps, lab), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_repro.add_contours(image.index_img(atlas.maps, lab), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	    elif i in [2,3]:
	        atlas = atlas_sub
	        lab_right = 10
	        lab_left = 20
	        display_orig.add_contours(image.index_img(atlas.maps, lab_right), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_orig.add_contours(image.index_img(atlas.maps, lab_left), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_repro.add_contours(image.index_img(atlas.maps, lab_right), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_repro.add_contours(image.index_img(atlas.maps, lab_left), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	    elif i in [6, 7, 8]:
	        atlas = atlas_sub
	        lab_right = 9
	        lab_left = 19
	        display_orig.add_contours(image.index_img(atlas.maps, lab_right), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_orig.add_contours(image.index_img(atlas.maps, lab_left), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_repro.add_contours(image.index_img(atlas.maps, lab_right), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	        display_repro.add_contours(image.index_img(atlas.maps, lab_left), contours=1, antialiased=True,
	                     linewidths=2., levels=[0], colors=['green'])
	    f.savefig(opj(figures_dir, f"thresholded_roi_spm_hypo{i+1}_{team_ID}.png"))



def plot_unthresholded_statmap_comparisons(original_unthresh, repro_unthresh, hypothesis, figures_dir, team_ID, vmPFC, vs, amygdala):
	for i in range(len(original_unthresh)):
	    if i in [0,1,4,5]:
	        coords_cut = vmPFC
	    elif i in [2,3]:
	        coords_cut = vs
	    else:
	        coords_cut = amygdala
	    
	    f = plt.figure(figsize = (15, 7))
	    gs = f.add_gridspec(2, 1)
	    ax1 = f.add_subplot(gs[0, 0])
	    ax2 = f.add_subplot(gs[1, 0])
	    f.suptitle(f"Unthresholded statistical maps for Hypothesis {i+1} \n {hypothesis[i]}",
	               fontweight = 'bold')
	    plotting.plot_stat_map(original_unthresh[i], title = f"Original by Team {team_ID}", 
	                        cut_coords = coords_cut, figure = f, axes = ax1, colorbar = True)
	    plotting.plot_stat_map(repro_unthresh[i], title = f"Reproduction of Team {team_ID} pipeline",
	                        cut_coords = coords_cut, figure = f, axes = ax2, colorbar = True)
	    f.savefig(opj(figures_dir, f"unthresholded_spm_hypo{i+1}_{team_ID}.png"))

def plot_unthresholded_statmap_analytical_variability(repro_unthresh, n_val, teams, result_dir, coords_cut):
    f = plt.figure(figsize = (30, 25), constrained_layout=True)
    gs = f.add_gridspec(nrows=8, ncols=5)
    ax1 = f.add_subplot(gs[0, 0])
    ax2 = f.add_subplot(gs[1, 0])
    ax3 = f.add_subplot(gs[2, 0])
    ax4 = f.add_subplot(gs[3, 0])
    ax5 = f.add_subplot(gs[4, 0])
    ax6 = f.add_subplot(gs[5, 0])
    ax7 = f.add_subplot(gs[6, 0])
    ax8 = f.add_subplot(gs[7, 0])
    ax9 = f.add_subplot(gs[0, 1]) 
    ax10 = f.add_subplot(gs[1, 1]) 
    ax11 = f.add_subplot(gs[2, 1])
    ax12 = f.add_subplot(gs[3, 1])
    ax13 = f.add_subplot(gs[4, 1])
    ax14 = f.add_subplot(gs[5, 1])
    ax15 = f.add_subplot(gs[6, 1])
    ax16 = f.add_subplot(gs[7, 1])
    ax17 = f.add_subplot(gs[0, 2]) 
    ax18 = f.add_subplot(gs[1, 2]) 
    ax19 = f.add_subplot(gs[2, 2])
    ax20 = f.add_subplot(gs[3, 2])
    ax21 = f.add_subplot(gs[4, 2])
    ax22 = f.add_subplot(gs[5, 2])
    ax23 = f.add_subplot(gs[6, 2])
    ax24 = f.add_subplot(gs[7, 2])
    ax25 = f.add_subplot(gs[0, 3]) 
    ax26 = f.add_subplot(gs[1, 3]) 
    ax27 = f.add_subplot(gs[2, 3])
    ax28 = f.add_subplot(gs[3, 3])
    ax29 = f.add_subplot(gs[4, 3])
    ax30 = f.add_subplot(gs[5, 3])
    ax31 = f.add_subplot(gs[6, 3])
    ax32 = f.add_subplot(gs[7, 3])
    ax33 = f.add_subplot(gs[0, 4]) 
    ax34 = f.add_subplot(gs[1, 4]) 
    ax35 = f.add_subplot(gs[2, 4])
    ax36 = f.add_subplot(gs[3, 4])
    ax37 = f.add_subplot(gs[4, 4])
    ax38 = f.add_subplot(gs[5, 4])
    ax39 = f.add_subplot(gs[6, 4])
    ax40 = f.add_subplot(gs[7, 4])
    axs = [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11, ax12, ax13, ax14, ax15, ax16, ax17, ax18, ax19, ax20,
          ax21, ax22, ax23, ax24, ax25, ax26, ax27, ax28, ax29, ax30, ax31, ax32, ax33, ax34, ax35, ax36, ax37, ax38, ax39, ax40]
    plt.style.use('default')
    axe = 0
    for i_n, n in enumerate(n_val):
        for i_t, team_ID in enumerate(teams): 
            spm = axs[axe]
            axe += 1
            #spm.set_title(f"Reproduction of team {team_ID} \n with {n} subjects for hypothesis 5", fontweight = 'bold')
            display = plotting.plot_stat_map(repro_unthresh[team_ID][str(n)], cut_coords = [coords_cut[2]],
                                             figure = f, axes = spm, colorbar = True, vmax = 5, display_mode = 'z')
    f.savefig(opj(result_dir, f"unthresh_spm_hypo_5.png"))

def plot_statmap_rois_analytical_variability(repro_thresh, n_val, teams, result_dir, coords_cut, atlas, lab):
    f = plt.figure(figsize = (30, 25), constrained_layout=True)
    gs = f.add_gridspec(nrows=8, ncols=5)
    ax1 = f.add_subplot(gs[0, 0])
    ax2 = f.add_subplot(gs[1, 0])
    ax3 = f.add_subplot(gs[2, 0])
    ax4 = f.add_subplot(gs[3, 0])
    ax5 = f.add_subplot(gs[4, 0])
    ax6 = f.add_subplot(gs[5, 0])
    ax7 = f.add_subplot(gs[6, 0])
    ax8 = f.add_subplot(gs[7, 0])
    ax9 = f.add_subplot(gs[0, 1]) 
    ax10 = f.add_subplot(gs[1, 1]) 
    ax11 = f.add_subplot(gs[2, 1])
    ax12 = f.add_subplot(gs[3, 1])
    ax13 = f.add_subplot(gs[4, 1])
    ax14 = f.add_subplot(gs[5, 1])
    ax15 = f.add_subplot(gs[6, 1])
    ax16 = f.add_subplot(gs[7, 1])
    ax17 = f.add_subplot(gs[0, 2]) 
    ax18 = f.add_subplot(gs[1, 2]) 
    ax19 = f.add_subplot(gs[2, 2])
    ax20 = f.add_subplot(gs[3, 2])
    ax21 = f.add_subplot(gs[4, 2])
    ax22 = f.add_subplot(gs[5, 2])
    ax23 = f.add_subplot(gs[6, 2])
    ax24 = f.add_subplot(gs[7, 2])
    ax25 = f.add_subplot(gs[0, 3]) 
    ax26 = f.add_subplot(gs[1, 3]) 
    ax27 = f.add_subplot(gs[2, 3])
    ax28 = f.add_subplot(gs[3, 3])
    ax29 = f.add_subplot(gs[4, 3])
    ax30 = f.add_subplot(gs[5, 3])
    ax31 = f.add_subplot(gs[6, 3])
    ax32 = f.add_subplot(gs[7, 3])
    ax33 = f.add_subplot(gs[0, 4]) 
    ax34 = f.add_subplot(gs[1, 4]) 
    ax35 = f.add_subplot(gs[2, 4])
    ax36 = f.add_subplot(gs[3, 4])
    ax37 = f.add_subplot(gs[4, 4])
    ax38 = f.add_subplot(gs[5, 4])
    ax39 = f.add_subplot(gs[6, 4])
    ax40 = f.add_subplot(gs[7, 4])
    axs = [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11, ax12, ax13, ax14, ax15, ax16, ax17, ax18, ax19, ax20,
          ax21, ax22, ax23, ax24, ax25, ax26, ax27, ax28, ax29, ax30, ax31, ax32, ax33, ax34, ax35, ax36, ax37, ax38, ax39, ax40]
    plt.style.use('default')
    axe = 0
    for i_n, n in enumerate(n_val):
        for i_t, team_ID in enumerate(teams): 
            spm = axs[axe]
            axe += 1
            #spm.set_title(f"Reproduction of team {team_ID} \n with {n} subjects for hypothesis 5",
            #           fontweight = 'bold')

            display = plotting.plot_stat_map(repro_thresh[team_ID][str(n)], cut_coords = coords_cut,
                                             figure = f, axes = spm, colorbar = True, vmax = 10)

            display.add_contours(image.index_img(atlas.maps, lab), contours=1, antialiased=True,
                         linewidths=2., levels=[0], colors=['green'])

    f.savefig(opj(result_dir, f"thresh_spm_hypo_5.png"))

def plot_statmap_gb_analytical_variability(repro_thresh, n_val, teams, result_dir, coords_cut, atlas, lab):
    f = plt.figure(figsize = (30, 25), constrained_layout=True)
    gs = f.add_gridspec(nrows=8, ncols=5)
    ax1 = f.add_subplot(gs[0, 0])
    ax2 = f.add_subplot(gs[1, 0])
    ax3 = f.add_subplot(gs[2, 0])
    ax4 = f.add_subplot(gs[3, 0])
    ax5 = f.add_subplot(gs[4, 0])
    ax6 = f.add_subplot(gs[5, 0])
    ax7 = f.add_subplot(gs[6, 0])
    ax8 = f.add_subplot(gs[7, 0])
    ax9 = f.add_subplot(gs[0, 1]) 
    ax10 = f.add_subplot(gs[1, 1]) 
    ax11 = f.add_subplot(gs[2, 1])
    ax12 = f.add_subplot(gs[3, 1])
    ax13 = f.add_subplot(gs[4, 1])
    ax14 = f.add_subplot(gs[5, 1])
    ax15 = f.add_subplot(gs[6, 1])
    ax16 = f.add_subplot(gs[7, 1])
    ax17 = f.add_subplot(gs[0, 2]) 
    ax18 = f.add_subplot(gs[1, 2]) 
    ax19 = f.add_subplot(gs[2, 2])
    ax20 = f.add_subplot(gs[3, 2])
    ax21 = f.add_subplot(gs[4, 2])
    ax22 = f.add_subplot(gs[5, 2])
    ax23 = f.add_subplot(gs[6, 2])
    ax24 = f.add_subplot(gs[7, 2])
    ax25 = f.add_subplot(gs[0, 3]) 
    ax26 = f.add_subplot(gs[1, 3]) 
    ax27 = f.add_subplot(gs[2, 3])
    ax28 = f.add_subplot(gs[3, 3])
    ax29 = f.add_subplot(gs[4, 3])
    ax30 = f.add_subplot(gs[5, 3])
    ax31 = f.add_subplot(gs[6, 3])
    ax32 = f.add_subplot(gs[7, 3])
    ax33 = f.add_subplot(gs[0, 4]) 
    ax34 = f.add_subplot(gs[1, 4]) 
    ax35 = f.add_subplot(gs[2, 4])
    ax36 = f.add_subplot(gs[3, 4])
    ax37 = f.add_subplot(gs[4, 4])
    ax38 = f.add_subplot(gs[5, 4])
    ax39 = f.add_subplot(gs[6, 4])
    ax40 = f.add_subplot(gs[7, 4])
    axs = [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11, ax12, ax13, ax14, ax15, ax16, ax17, ax18, ax19, ax20,
          ax21, ax22, ax23, ax24, ax25, ax26, ax27, ax28, ax29, ax30, ax31, ax32, ax33, ax34, ax35, ax36, ax37, ax38, ax39, ax40]
    plt.style.use('default')
    axe = 0
    for i_n, n in enumerate(n_val):
        for i_t, team_ID in enumerate(teams): 
            spm = axs[axe]
            axe += 1
            #spm.set_title(f"Reproduction of team {team_ID} \n with {n} subjects for hypothesis 5",
            #           fontweight = 'bold')

            display = plotting.plot_glass_brain(repro_thresh[team_ID][str(n)],
                                             figure = f, axes = spm, colorbar = True, vmax = 10, display_mode = 'z')

            display.add_contours(image.index_img(atlas.maps, lab), contours=1, antialiased=True,
                         linewidths=2., levels=[0], colors=['green'])

    f.savefig(opj(result_dir, f"thresh_gb_hypo_5.png"))
    
    
def plot_statmap_unthresh_gb_analytical_variability(repro_thresh, repro_unthresh, n_val, teams, result_dir, coords_cut, atlas, lab):
    f = plt.figure(figsize = (30, 25))
    gs = f.add_gridspec(nrows=8, ncols=10)

    plt.style.use('default')
    axe = 0
    axe2 = 35
    for i_n, n in enumerate(n_val):
        for i_t, team_ID in enumerate(teams): 
            spm = f.add_subplot(gs[i_t, 2*i_n+1])
            #spm.set_title(f"Reproduction of team {team_ID} \n with {n} subjects for hypothesis 5",
            #           fontweight = 'bold')

            display = plotting.plot_glass_brain(repro_thresh[team_ID][str(n)],
                                             figure = f, axes = spm, colorbar = True, vmax = 8, display_mode = 'z')

            display.add_contours(image.index_img(atlas.maps, lab), contours=1, antialiased=True,
                         linewidths=2., levels=[0], colors=['green'])
            spm = f.add_subplot(gs[i_t, 2*i_n])
            axe += 1
            
            plotting.plot_glass_brain(repro_unthresh[team_ID][str(n)], #cut_coords = [coords_cut[2]],
                                             figure = f, axes = spm, vmin=-8, vmax = 8, display_mode = 'z',
                                             plot_abs=False, colorbar=True, cmap=nilearn_cmaps['cold_hot'], 
                                     threshold=1e-6)

    f.savefig(opj(result_dir, f"unthresh_thresh_gb_hypo_5.png"))
    
    
def plot_statmap_unthresh_thresh_analytical_variability(repro_thresh, repro_unthresh, n_val, teams, result_dir, coords_cut, atlas, lab):
    f = plt.figure(figsize = (30, 25))
    gs = f.add_gridspec(nrows=8, ncols=10)
    plt.style.use('default')
    axe = 0
    axe2 = 35
    for i_n, n in enumerate(n_val):
        for i_t, team_ID in enumerate(teams): 
            spm = f.add_subplot(gs[i_t, 2*i_n+1])

            display = plotting.plot_stat_map(repro_thresh[team_ID][str(n)],
                                             figure = f, axes = spm, colorbar = False, vmax = 8, display_mode = 'z', cut_coords=[coords_cut[2]], annotate=False, draw_cross=False)

            display.add_contours(image.index_img(atlas.maps, lab), contours=1, antialiased=True,
                         linewidths=2., levels=[0], colors=['green'])
            spm = f.add_subplot(gs[i_t, 2*i_n])
            
            plotting.plot_stat_map(repro_unthresh[team_ID][str(n)], #cut_coords = [coords_cut[2]],
                                             figure = f, axes = spm, vmax = 8, display_mode = 'z',
                                             colorbar=False, cut_coords=[coords_cut[2]], annotate=False, draw_cross=False)

    f.savefig(opj(result_dir, f"unthresh_thresh_maps_hypo_5.png"))


