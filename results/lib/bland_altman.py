import nibabel as nib
from nibabel.processing import resample_from_to
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
from matplotlib.axes import Axes
import os

def mask_using_nan(data_img):
    # Set masking using NaN's
    data_orig = data_img.get_fdata()

    if np.any(np.isnan(data_orig)):
        # Already using NaN
        data_img_nan = data_img
    else:
        # Replace zeros by NaNs
        data_nan = data_orig
        data_nan[data_nan == 0] = np.nan
        # Save as image
        data_img_nan = nib.Nifti1Image(data_nan, data_img.affine)

    return(data_img_nan)


def bland_altman_values(data1_file, data2_file, reslice_on_2=True):

    # Load nifti images
    data1_img = nib.load(data1_file)
    data2_img = nib.load(data2_file)

    # Set masking using NaN's
    data1_img = mask_using_nan(data1_img)
    data2_img = mask_using_nan(data2_img)

    if reslice_on_2:
        # Resample data1 on data2 using nearest nneighbours
        data1_resl_img = resample_from_to(data1_img, data2_img, order=0)

        # Load data from images
        data1 = data1_resl_img.get_fdata()
        data2 = data2_img.get_fdata()
    else:
        # Resample data2 on data1 using nearest nneighbours
        data2_resl_img = resample_from_to(data2_img, data1_img, order=0)

        # Load data from images
        data1 = data1_img.get_fdata()
        data2 = data2_resl_img.get_fdata()

    # Vectorise input data
    data1 = np.reshape(data1, -1)
    data2 = np.reshape(data2, -1)


    in_mask_indices = np.logical_not(
        np.logical_or(
            np.logical_or(np.isnan(data1), np.absolute(data1) == 0),
            np.logical_or(np.isnan(data2), np.absolute(data2) == 0)))

    data1 = data1[in_mask_indices]
    data2 = data2[in_mask_indices]

    mean = np.mean([data1, data2], axis=0)
    diff = data1 - data2  # Difference between data1 and data2

    md = np.mean(diff)                   # Mean of the difference
    sd = np.std(diff, axis=0)            # Standard deviation of the difference

    return mean, diff, md, sd


def bland_altman_plot(f, gs, stat_file_1, stat_file_2, title, x_lab, y_lab,
                      reslice_on_2=True, lims=(-10,10,-8,8)):
    
    mean, diff, md, sd = bland_altman_values(
        stat_file_1, stat_file_2, reslice_on_2)
    
    # Figure centrale 
    ax1 = f.add_subplot(gs[:-1, 1:5])
    hb = ax1.hexbin(mean, diff, bins='log', cmap='viridis', gridsize=50, extent=lims)
    ax1.axis(lims)
    ax1.axhline(linewidth=1, color='r')
    ax1.set_title(title)
    
    # Histogram des différences 
    ax2 = f.add_subplot(gs[:-1, 0], xticklabels=[], sharey=ax1)
    ax2.set_ylim(lims[2:4])
    ax2.hist(diff, 100, range=lims[2:4],histtype='stepfilled',
             orientation='horizontal', color='gray')
    ax2.invert_xaxis()
    ax2.set_ylabel('Difference' + y_lab)
    
    # Histogram des moyennes
    ax3 = f.add_subplot(gs[-1, 1:5], yticklabels=[], sharex=ax1)
    ax3.hist(mean, 100, range=lims[0:2],histtype='stepfilled',
             orientation='vertical', color='gray')
    ax3.set_xlim(lims[0:2])
    ax3.invert_yaxis()
    ax3.set_xlabel('Average' + x_lab)
    
    # Color bar
    ax4 = f.add_subplot(gs[:-1, 5])
    ax4.set_aspect(20)
    pos1 = ax4.get_position()
    ax4.set_position([pos1.x0 - 0.025, pos1.y0, pos1.width, pos1.height])
    cb = f.colorbar(hb, cax=ax4)
    cb.set_label('log10(N)')