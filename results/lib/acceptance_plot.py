import matplotlib.pyplot as plt 
import numpy as np
from os.path import join as opj

def plot_acceptance_proportion(teams, n_val, result_dir):
	answers_hypo = {str(n) : [] for n in n_val}

	for team_ID in teams:
	    for i_n, n in enumerate(n_val):
	        a = input(f"Answer for team {team_ID} with {str(n)} subject for hypothesis 5: ")
	        answers_hypo[str(n)].append(a)

	teams_proportion = {}
	teams_proportion.update({str(n) : [] for n in n_val})

	for n in n_val:
	    for i, i_n in enumerate(answers_hypo[str(n)]):
	        if i_n != '1' and i_n != '0' and i_n != 1 and i_n != 0 :
	            answers_hypo[str(n)][i] = 1
	    prop = sum(list(map(int, answers_hypo[str(n)])))/len(answers_hypo[str(n)])
	    teams_proportion[str(n)].append(prop)

	f = plt.figure(figsize = (10, 7))
	plt.scatter(n_val,
               [teams_proportion[str(i)] for i in n_val], marker = 'x', s=200)
	plt.plot(n_val,
               [teams_proportion[str(i)] for i in n_val])
    #plt.bar(n_val,
    #           [teams_proportion['20'][i], teams_proportion['40'][i], teams_proportion['60'][i], 
    #           teams_proportion['108'][i]])
	plt.grid()
	plt.title(f"Proportion of reproduced maps computed with different sample size \n allowing to give a positive answer to Hypothesis 5.")
	plt.ylabel("Proportion of teams")
	plt.xlabel("Sample size")

	f.savefig(opj(result_dir, f"prop_team_h5.png"))