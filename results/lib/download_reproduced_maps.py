from os.path import join as opj
import os
import urllib.request
from zipfile import ZipFile
import shutil
from glob import glob

def get_reproduced_maps(collection, teamID, data_dir):
    url = "http://neurovault.org/collections/" + collection + '/download' 
    local_file = opj(data_dir, str('NARPS-' + teamID + '-reproduced.zip'))
    local_file_unzip = opj(data_dir, str('NARPS-' + teamID + '-reproduced'))
    if not os.path.isdir(data_dir):
        os.mkdir(data_dir)
    if not os.path.isfile(local_file) or not os.path.isdir(local_file_unzip):
        # Copy file locally in a the data directory
        urllib.request.urlretrieve(url, local_file)
        print("downloading " + url + " at " + local_file)
        
        with ZipFile(local_file, 'r') as zip:
            print("unzipping" + local_file)
            zip.extractall(data_dir)

    else:
        print(url + " already downloaded at " + local_file)

    for dirs in glob(opj(data_dir, "*")):
        if os.path.isdir(dirs) and teamID in dirs:
            os.rename(dirs, opj(data_dir, f"NARPS-{teamID}-reproduced"))

    file_list = glob(opj(data_dir, f"NARPS-{teamID}-reproduced", '*'))
    return file_list 


def get_all_reproduced_maps(repro_dir, local_filepath = 'NARPS-reproduction.zip'):
    from os.path import join as opj
    import os
    url = "http://neurovault.org/collections/11971/download"
    local_file = opj(repro_dir, local_filepath)
    local_file_unzip = local_file[:-4]
    
    if not os.path.isdir(repro_dir):
        os.mkdir(repro_dir)
    if not os.path.isfile(local_file) or not os.path.isdir(local_file_unzip):
        # Copy file locally in a the data directory
        urllib.request.urlretrieve(url, local_file)
        print("downloading " + url + " at " + local_file)

        with ZipFile(local_file, 'r') as zip:
            print("unzipping " + local_file)
            zip.extractall(repro_dir)

    else:
        print(url + " already downloaded at " + local_file)
