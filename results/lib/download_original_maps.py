from os.path import join as opj
import os
import urllib.request
from zipfile import ZipFile
from glob import glob

def get_original_maps(collection, teamID, data_dir):
    url = "http://neurovault.org/collections/" + collection + '/download' 
    local_file = opj(data_dir, str('NARPS-' + teamID + '.zip'))
    local_file_unzip = opj(data_dir, str('NARPS - ' + teamID))
    if not os.path.isdir(data_dir):
        os.mkdir(data_dir)
    if not os.path.isfile(local_file) or not os.path.isdir(local_file_unzip):
        # Copy file locally in a the data directory
        urllib.request.urlretrieve(url, local_file)
        print("downloading " + url + " at " + local_file)
        
        with ZipFile(local_file, 'r') as zip:
            print("unzipping" + local_file)
            zip.extractall(data_dir)

    else:
        print(url + " already downloaded at " + local_file)

    for dirs in glob(opj(data_dir, "*")):
        if dirs[-4:] == teamID:
            os.rename(dirs, opj(data_dir, f"NARPS-{teamID}"))

    file_list = glob(opj(data_dir, f"NARPS-{teamID}", '*'))
    return file_list 