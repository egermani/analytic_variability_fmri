import nibabel as nib
from nibabel.processing import resample_from_to
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.axes import Axes
import os
from os.path import join as opj
import pandas as pd


def mask_using_nan(data_img):
    # Set masking using NaN's
    data_orig = data_img.get_fdata()

    if np.any(np.isnan(data_orig)):
        # Already using NaN
        data_img_nan = data_img
    else:
        # Replace zeros by NaNs
        data_nan = data_orig
        data_nan[data_nan == 0] = np.nan
        # Save as image
        data_img_nan = nib.Nifti1Image(data_nan, data_img.affine)

    return(data_img_nan)


def get_masked_vectorised_data(data1_file, data2_file, reslice_on_2 = True):
        # Load nifti images
    data1_img = nib.load(data1_file)
    data2_img = nib.load(data2_file)

    # Set masking using NaN's
    data1_img = mask_using_nan(data1_img)
    data2_img = mask_using_nan(data2_img)

    if reslice_on_2:
        # Resample data1 on data2 using nearest nneighbours
        data1_resl_img = resample_from_to(data1_img, data2_img, order=0)

        # Load data from images
        data1 = data1_resl_img.get_fdata()
        data2 = data2_img.get_fdata()
    else:
        # Resample data2 on data1 using nearest nneighbours
        data2_resl_img = resample_from_to(data2_img, data1_img, order=0)

        # Load data from images
        data1 = data1_img.get_fdata()
        data2 = data2_resl_img.get_fdata()

    # Vectorise input data
    data1 = np.reshape(data1, -1)
    data2 = np.reshape(data2, -1)

    in_mask_indices = np.logical_not(
        np.logical_or(
            np.logical_or(np.isnan(data1), np.absolute(data1) == 0),
            np.logical_or(np.isnan(data2), np.absolute(data2) == 0)))

    data1 = data1[in_mask_indices]
    data2 = data2[in_mask_indices]

    return data1, data2


def get_corr_coeff(data1_file, data2_file, method = 'pearson', reslice_on_2 = True):
    data1, data2 = get_masked_vectorised_data(data1_file, data2_file, reslice_on_2)
    
    if len(data1) == 0 or len(data2) == 0:
        return f"Images with 0 common activated voxels given. No metric calculable."
    
    if method == 'pearson':
        corr_coeff = np.corrcoef(data1, data2)[0][1]

    elif method == 'spearman':
        corr_coeff = stats.spearmanr(data1, data2).correlation

    return corr_coeff

def get_pairwise_correlation(teams, n_val, repro_unthresh):
    correlation_dict = {}

    for i in range(len(teams)-1):
        for j in range(i, len(teams)):
            if i != j:
                correlation_dict.update({f"{teams[i]}-{teams[j]}" : []})
                for n in n_val:
                    corr1 = get_corr_coeff(repro_unthresh[teams[i]][str(n)], 
                                                     repro_unthresh[teams[j]][str(n)], 
                                                      method = 'pearson', reslice_on_2 = True)
                    corr2 = get_corr_coeff(repro_unthresh[teams[j]][str(n)], 
                                                     repro_unthresh[teams[i]][str(n)], 
                                                      method = 'pearson', reslice_on_2 = True)
                    corr = (corr1 + corr2) / 2
                    correlation_dict[f"{teams[i]}-{teams[j]}"].append(corr)

    return correlation_dict

def get_mean_pairwise_correlation(correlation_dict, n_val):
    mean_corr_list = []

    for i, n in enumerate(n_val):
        mean_corr_list.append(np.mean(np.array(list(correlation_dict.values()))[:,i]))

    return mean_corr_list

def plot_correlation_analytical_variability(teams, n_val, repro_unthresh, result_dir):
    correlation_dict = get_pairwise_correlation(teams, n_val, repro_unthresh)

    mean_corr_list = get_mean_pairwise_correlation(correlation_dict, n_val)
    
    correlation_df = pd.DataFrame(list(zip(correlation_dict.keys(), correlation_dict.values())))
    correlation_df.to_csv(opj(result_dir, f"h5_corr_coeff.csv"))

    f = plt.figure(figsize = (10, 7))
    plt.title(f"Pairwise correlation between unthresholded maps \n for Hypotheses 5 depending on sample sizes.")
    plt.grid()
    for team_comp in correlation_dict.keys():
        plt.scatter(n_val, correlation_dict[team_comp])
        plt.plot(n_val, correlation_dict[team_comp], label = team_comp, ls = 'dotted')
    plt.scatter(n_val, mean_corr_list,color = 'red')
    plt.plot(n_val, mean_corr_list, label = 'Mean pairwise correlation', color = 'red', lw = 3)

    f.legend()
    f.savefig(opj(result_dir, f"h5_corrcoeff.png"))
    
    
    









