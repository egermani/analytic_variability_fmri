#INSTRUCTIONS TO DOWNLOAD DATA

## Original Data

### Excel file describing pipelines 

This excel file is only necessary if you want to run the `/src/select_pipelines.ipynb` notebook.

It allows selection of teams to reproduce. However, since teams to reproduce are selected randomly, you may not obtain the teams for which we reproduced the results. 

This excel file must be store in the `data` directory and can be found [**here**](https://github.com/poldrack/narps/blob/master/ImageAnalyses/metadata_files/analysis_pipelines_for_analysis.xlsx)

### NARPS dataset

The dataset used for the `/src/reproduction_*.ipynb` notebooks can be downloaded [**here**](https://openneuro.org/datasets/ds001734/versions/1.0.5).

The data must be stored in a `ds001734` directory inside the `data/original` directory. 

I recommand to download it with **Datalad**. If you want to use it, install [**Datalad**](http://handbook.datalad.org/en/latest/intro/installation.html#install), place yourself in the `data/original` directory and run `datalad install ///openneuro/ds001734`.
After, you can download all the files by running `datalad get ./*` and if you only want parts of the data, replace the * by the paths to the desired files. 

## Derived Data

Derived data such as original stat maps from teams and reproduced stat maps are automatically downloaded when running the `results/results_comparison_*.ipynb` notebooks. 

