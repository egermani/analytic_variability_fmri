## CREATION OF DOCKER IMAGE / DOCKER FILE WITH NECESSARY PACKAGES 
docker run --rm repronim/neurodocker:0.7.0 generate docker \
           --base neurodebian:stretch-non-free --pkg-manager apt \
           --install git \
           --fsl version=6.0.3 \
           --afni version=latest method=binaries install_r=true install_r_pkgs=true install_python2=true install_python3=true \
           --spm12 version=r7771 method=binaries \
           --user=neuro \
           --workdir /home \
           --miniconda create_env=neuro \
                       conda_install="python=3.8 traits jupyter nilearn graphviz nipype scikit-image" \
                       pip_install="matplotlib" \
                       activate=True \
           --env LD_LIBRARY_PATH="/opt/miniconda-latest/envs/neuro:$LD_LIBRARY_PATH" \
           --run-bash "source activate neuro" \
           --user=root \
           --run 'chmod 777 -Rf /home' \
           --run 'chown -R neuro /home' \
           --user=neuro \
           --run 'mkdir -p ~/.jupyter && echo c.NotebookApp.ip = \"0.0.0.0\" > ~/.jupyter/jupyter_notebook_config.py' > Dockerfile

## START HERE IF YOU WANT TO USE THE PROVIDED DOCKERFILE
### IMAGE BUILDING 
docker build --tag [name_of_the_image] - < Dockerfile

### FOR MAC APPLE SILICON USERS 
docker build --tag [name_of_the_image] --platform linux/amd64 - < Dockerfile

### RUN OF THE IMAGE IN A DOCKER CONTAINER  
docker run -ti -p 8888:8888 -v /home/egermani:/home [name_of_the_image]
#### WITH THE -V OPTION, WE CREATE A DATAVOLUME AND WE STORE THE REPOSITORY '/home/egermani' IN THE '/home' REPOSITORY OF THE CONTAINER
#### THE REPOSITORY TO CONNECT WITH THE DOCKER CONTAINER MUST BE CHANGED WITH YOUR WANTED ENVIRONEMENT PATH

### START THE CONTAINER 
docker start [name_of_the_container]

### VERIFY THE CONTAINER IS IN THE LIST 
docker ps 

### EXECUTE BASH OR ATTACH YOUR CONTAINER 
docker exec -ti [name_of_the_container] bash
OR
docker attach [name_of_the_container]

## INSIDE YOUR DOCKER CONTAINER : 
### ACTIVATE CONDA ENVIRONMENT
source activate neuro 

### LAUNCH JUPYTER NOTEBOOK
jupyter notebook --port=8888 --no-browser --ip=0.0.0.0

## **IF YOU CAN NOT MODIFY THE NOTEBOOKS** : 
### LAUNCH AN INTERACTIVE BASH TERMINAL USING ROOT
docker exec --tty --interactive --user root [name_of_the_container] bash
### ADD AUTHORIZATION FOR DIRECTORIES TO USER NEURO
(Command to launch in the docker terminal as root) 
cd / (OR cd .. until you reach directory /)
chmod -R 777 .
### THIS WILL RISE ERRORS YOU CAN IGNORE
### EXIT YOUR DOCKER CONTAINER 
exit

## IF YOU DID NOT USE YOUR CONTAINER FOR A WHILE : 
### VERIFY IT STILL RUN : 
docker ps -l 
### IF YOUR DOCKER CONTAINER IS IN THE LIST, RUN : 
docker start [name_of_the_container]
### ELSE, RERUN IT WITH : 
docker run -ti -p 8888:8888 -v /home/egermani:/home [name_of_the_image]

## TO USE SPM INSIDE A NEURODOCKER, USE THIS COMMANDS AT THE BEGINNING OF THE SCRIPT
from nipype.interfaces import spm
matlab_cmd = '/opt/spm12-r7771/run_spm12.sh /opt/matlabmcr-2010a/v713/ script'
spm.SPMCommand.set_mlab_paths(matlab_cmd=matlab_cmd, use_mcr=True)
