from lib import pipelines_2T6S, pipelines_C88N, pipelines_Q6O0, pipelines_X19V, pipelines_0I4U, pipelines_4TQ6, pipelines_V55J, pipelines_98BT, pipelines_T54A, pipelines_J7F9

import random
import os 
from os.path import join as opj
import sys
import getopt
import json

# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)
simplefilter(action='ignore', category=UserWarning)
simplefilter(action='ignore', category=RuntimeWarning)

if __name__ == "__main__":
    list_team_ID = []
    subject_list = []
    exp_dir = None
    result_dir = None
    data_dir = None
    operation = []

    try:
        OPTIONS, REMAINDER = getopt.getopt(sys.argv[1:], 'e:r:s:t:o:d:', ['exp_dir=', 'result_dir=', 'subjects=', 'team_ID=', 'operation=', 'data_dir='])

    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)

    # Replace variables depending on options
    for opt, arg in OPTIONS:
        if opt in ('-e', '--exp_dir'):
            exp_dir= arg
        elif opt in ('-r', '--result_dir'):
            result_dir = arg
        elif opt in ('-d', '--data_dir'):
            data_dir = arg
        elif opt in ('-s', '--subjects'):
            subject_list = json.loads(arg)
        elif opt in ('-t', '--team_ID'):
            list_team_ID.append(arg)
        elif opt in ('-o', '--operation'): 
            operation.append(arg) # preprocess, l1, l2


    print('OPTIONS   :', OPTIONS)

    if len(operation) == 0:
        print('All operations will be performed.')
        operation = ['preprocess', 'l1', 'l2', 'l3']


    if exp_dir is not None and result_dir is not None and len(list_team_ID) > 0 and len(subject_list) > 0: 
        #TR
        with open(opj(exp_dir, 'task-MGT_bold.json'), 'rt') as fp:
            task_info = json.load(fp)
        TR = task_info['RepetitionTime']
        ST = task_info['SliceTiming']
        ET = task_info["EffectiveEchoSpacing"]

        N = len(ST)
        TA = TR/N
        total_readout_time = N * ET

        run_list = ['01', '02', '03', '04']

        n_sub = len(subject_list)

        for team_ID in list_team_ID:

            ## working_dir : where the intermediate outputs will be store
            working_dir = f"NARPS-{team_ID}-reproduced/intermediate_results"

            ## output_dir : where the final results will be store
            output_dir = f"NARPS-{team_ID}-reproduced"

            if team_ID == "2T6S":
                #FWHM to smooth (team chose a kernel of 8mm for smoothing)
                fwhm = 8

                contrast_list = ['01', '02', '03', '04']

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_2T6S = pipelines_2T6S.get_l1_analysis(subject_list, TR, fwhm, run_list, exp_dir, 
                                                 result_dir, working_dir, output_dir)

                    l1_analysis_2T6S.run('MultiProc', plugin_args={'n_procs': 8})

                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    l2_analysis_equal_indiff = pipelines_2T6S.get_l2_analysis(subject_list, n_sub, contrast_list, "equalIndifference", 
                                                              exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equal_indiff.run('MultiProc', plugin_args={'n_procs': 8})

                    l2_analysis_equal_range = pipelines_2T6S.get_l2_analysis(subject_list, n_sub, contrast_list, "equalRange", 
                                                          exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equal_range.run('MultiProc', plugin_args={'n_procs': 8})

                    l2_analysis_groupcomp = pipelines_2T6S.get_l2_analysis(subject_list, n_sub, contrast_list, "groupComp", 
                                                          exp_dir, result_dir, working_dir, output_dir)


                    l2_analysis_groupcomp.run('MultiProc', plugin_args={'n_procs': 8})

                    l2_analysis_groupcomp.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_2T6S.reorganize_results(result_dir, output_dir, n_sub, team_ID)

            if team_ID == "C88N":
                fwhm = 8

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_C88N = pipelines_C88N.get_l1_analysis(subject_list, TR, 
                        fwhm, run_list, exp_dir, result_dir, working_dir, output_dir)

                    l1_analysis_C88N.run('MultiProc', plugin_args={'n_procs': 8})

                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    model_list = ["gain", "loss"]
                    contrast_list = ["1"]

                    l2_analysis_equalRange = pipelines_C88N.get_l2_analysis(subject_list, n_sub, model_list, contrast_list, 
                                                                            "equalRange", exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equalIndiff = pipelines_C88N.get_l2_analysis(subject_list, n_sub, model_list, contrast_list, 
                                                                             "equalIndifference", exp_dir, result_dir, working_dir, 
                                                                             output_dir)

                    model_list = ["loss"]
                    contrast_list = ["1"]

                    l2_analysis_groupComp = pipelines_C88N.get_l2_analysis(subject_list, n_sub, model_list, contrast_list, 
                                                                           "groupComp", exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 8})

                    l2_analysis_equalIndiff.run('MultiProc', plugin_args={'n_procs': 8})

                    l2_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})

                    model_list = ["loss"]
                    contrast_list = ["2"]

                    l2_analysis_equalRange = pipelines_C88N.get_l2_analysis(subject_list, n_sub, model_list, contrast_list, 
                                                                            "equalRange", exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equalIndiff = pipelines_C88N.get_l2_analysis(subject_list, n_sub, model_list, contrast_list, 
                                                                             "equalIndifference", exp_dir, result_dir, working_dir, 
                                                                             output_dir)

                    l2_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 8})

                    l2_analysis_equalIndiff.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_C88N.reorganize_results(result_dir, output_dir, n_sub, team_ID)

            if team_ID == "Q6O0":

                fwhm = 8

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_Q6O0 = pipelines_Q6O0.get_l1_analysis(subject_list, TR, fwhm, run_list, 
                                             exp_dir, result_dir, working_dir, output_dir)

                    l1_analysis_Q6O0.run('MultiProc', plugin_args={'n_procs': 8})

                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    model_list = ['gain', 'loss']
                    method = "equalRange"

                    l2_analysis_equal_range = pipelines_Q6O0.get_l2_analysis(subject_list, n_sub, model_list, method, 
                                                                 exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equal_range.run('MultiProc', plugin_args={'n_procs': 8})

                    model_list = ['gain', 'loss']
                    method = "equalIndifference"

                    l2_analysis_equal_indiff = pipelines_Q6O0.get_l2_analysis(subject_list, n_sub, model_list, method, exp_dir, 
                                                                 result_dir, working_dir, output_dir)

                    l2_analysis_equal_indiff.run('MultiProc', plugin_args={'n_procs': 8})

                    model_list = ['loss']
                    method = "groupComp"

                    l2_analysis_groupComp = pipelines_Q6O0.get_l2_analysis(subject_list, n_sub, model_list, method, exp_dir, 
                                                                 result_dir, working_dir, output_dir)

                    l2_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_Q6O0.reorganize_results(result_dir, output_dir, n_sub, team_ID)

            if team_ID == "0I4U":
                fwhm = 5

                if not os.path.isdir(opj(result_dir, output_dir,"preprocess", f"_subject_id_{subject_list[-1]}")) and 'preprocess' in operation:

                    preprocessing_0I4U = pipelines_0I4U.get_preprocessing(exp_dir, result_dir, working_dir, output_dir, subject_list, 
                        run_list, fwhm, TR, total_readout_time)

                    preprocessing_0I4U.run('MultiProc', plugin_args={'n_procs': 8})

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_0I4U = pipelines_0I4U.get_l1_analysis(subject_list, TR, run_list, exp_dir, result_dir,
                                             working_dir, output_dir)

                    l1_analysis_0I4U.run('MultiProc', plugin_args={'n_procs': 16})

                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    contrast_list = ['01', '02']
                    method = 'equalRange'

                    l2_analysis_equalRange = pipelines_0I4U.get_l2_analysis(subject_list, n_sub, contrast_list, method, exp_dir, 
                                                                            result_dir, working_dir, output_dir)

                    l2_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 16})

                    contrast_list = ['01', '02']
                    method = 'equalIndifference'

                    l2_analysis_equalIndifference = pipelines_0I4U.get_l2_analysis(subject_list, n_sub, contrast_list, method, 
                                                                                   exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equalIndifference.run('MultiProc', plugin_args={'n_procs': 20})

                    contrast_list = ['02']
                    method = 'groupComp'

                    l2_analysis_groupComp = pipelines_0I4U.get_l2_analysis(subject_list, n_sub, contrast_list, method, exp_dir, 
                                                                           result_dir, working_dir, output_dir)

                    l2_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 20})

                pipelines_0I4U.reorganize_results(result_dir, output_dir, n_sub, team_ID)

            if team_ID == "V55J":
                #FWHM to smooth (team chose a kernel of 6mm for smoothing)
                fwhm = 6

                if not os.path.isdir(opj(result_dir, output_dir,"preprocess", f"_subject_id_{subject_list[-1]}")) and 'preprocess' in operation:

                    preprocessing_V55J = pipelines_V55J.get_preprocessing(exp_dir, result_dir, working_dir, 
                                                 output_dir, subject_list, run_list, fwhm)

                    preprocessing_V55J.run('MultiProc', plugin_args={'n_procs': 16})

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_V55J = pipelines_V55J.get_l1_analysis(subject_list, TR, run_list, exp_dir, result_dir, working_dir, output_dir)

                    l1_analysis_V55J.run('MultiProc', plugin_args={'n_procs': 16})

                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    contrast_list = ['01', '02']
                    l2_analysis_equal_indiff = pipelines_V55J.get_l2_analysis(subject_list, n_sub, contrast_list, "equalIndifference", exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equal_indiff.run('MultiProc', plugin_args={'n_procs': 16})

                    l2_analysis_equal_range = pipelines_V55J.get_l2_analysis(subject_list, n_sub, contrast_list, "equalRange", exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equal_range.run('MultiProc', plugin_args={'n_procs': 16})

                    contrast_list = ['02']
                    method = 'groupComp'

                    l2_analysis_groupcomp = pipelines_V55J.get_l2_analysis(subject_list, n_sub, contrast_list, "groupComp", 
                                      exp_dir, result_dir, working_dir, output_dir)


                    l2_analysis_groupcomp.run('MultiProc', plugin_args={'n_procs': 16})

                pipelines_V55J.reorganize_results(result_dir, output_dir, n_sub, team_ID)
                
            if team_ID == "98BT":
                fwhm = 8
                
                if not os.path.isdir(opj(result_dir, output_dir, 'dartel_template')) and 'preprocess' in operation:
                    dartel_template_98BT = pipelines_98BT.get_dartel_template_wf(exp_dir, result_dir, working_dir, output_dir, subject_list)
                    dartel_template_98BT.run('MultiProc', plugin_args={'n_procs': 16})
                    
                if not os.path.isdir(opj(result_dir, output_dir,"preprocessing", f"_run_id_01_subject_id_{subject_list[-1]}")) and 'preprocess' in operation:
                    preprocessing_98BT = pipelines_98BT.get_preprocessing(exp_dir, result_dir, working_dir, output_dir, subject_list, run_list, fwhm, N, ST, TA, TR, total_readout_time)
                    
                    preprocessing_98BT.run('MultiProc', plugin_args={'n_procs': 16})
                
                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation: 
                    l1_analysis_98BT = pipelines_98BT.get_l1_analysis(subject_list, TR, run_list, exp_dir, result_dir, working_dir, output_dir)
                    
                    l1_analysis_98BT.run('MultiProc', plugin_args={'n_procs': 16})
                    
                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    contrast_list = ['01', '02', '03', '04']
                    method = 'equalRange'

                    l2_analysis_equalRange = pipelines_98BT.get_l2_analysis(subject_list, n_sub, contrast_list, method, exp_dir, 
                                                                            result_dir, working_dir, output_dir)

                    l2_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 8})

                    contrast_list = ['01', '02', '03', '04']
                    method = 'equalIndifference'

                    l2_analysis_equalIndifference = pipelines_98BT.get_l2_analysis(subject_list, n_sub, contrast_list, method, 
                                                                                   exp_dir, result_dir, working_dir, output_dir)

                    l2_analysis_equalIndifference.run('MultiProc', plugin_args={'n_procs': 8})

                    contrast_list = ['02', '04']
                    method = 'groupComp'

                    l2_analysis_groupComp = pipelines_98BT.get_l2_analysis(subject_list, n_sub, contrast_list, method, exp_dir, 
                                                                           result_dir, working_dir, output_dir)

                    l2_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_98BT.reorganize_results(result_dir, output_dir, n_sub, team_ID)
                
            if team_ID == "J7F9":
                fwhm = 8
                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_J7F9 = pipelines_J7F9.get_l1_analysis(subject_list, TR, fwhm, run_list, exp_dir, 
                                                 result_dir, working_dir, output_dir)

                    l1_analysis_J7F9.run('MultiProc', plugin_args={'n_procs': 4})
                    
                if not os.path.isdir(opj(result_dir, output_dir, f"l2_analysis_equalRange_nsub_{n_sub}")) and 'l2' in operation:

                    contrast_list = ['loss', 'gain']
                    method = 'equalRange'

                    l2_analysis_equalRange = pipelines_J7F9.get_l2_analysis(subject_list, n_sub, contrast_list, 'equalRange',
                                                                  exp_dir, output_dir, working_dir, result_dir, data_dir)

                    l2_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 8})

                    contrast_list = ['loss', 'gain']
                    method = 'equalIndifference'

                    l2_analysis_equalIndiff = pipelines_J7F9.get_l2_analysis(subject_list, n_sub, contrast_list, 
                                                                                'equalIndifference', exp_dir, output_dir, 
                                                                                working_dir, result_dir, data_dir)

                    l2_analysis_equalIndiff.run('MultiProc', plugin_args={'n_procs': 8})

                    contrast_list = ['loss']
                    method = 'groupComp'

                    l2_analysis_groupComp = pipelines_J7F9.get_l2_analysis(subject_list, n_sub, contrast_list, "groupComp", 
                                                                              exp_dir, output_dir, working_dir, 
                                                                              result_dir, data_dir)

                    l2_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_J7F9.reorganize_results(result_dir, output_dir, n_sub, team_ID)
                
            if team_ID == "T54A":
                if not os.path.isdir(opj(result_dir, output_dir, f"l1_analysis", f'_subject_id_{subject_list[-1]}_run_id_04')) and 'l1' in operation:
                    run_list = ['01', '02', '03', '04']

                    fwhm = 4
                    l1_analysis = pipelines_T54A.get_l1_analysis(subject_list, run_list, 
                                                                TR, fwhm, exp_dir, output_dir, 
                                                                working_dir, result_dir)
                    
                    l1_analysis.run('MultiProc', plugin_args={'n_procs': 4})
                    
                    contrast_list = ['1', '2']
                    
                    l2_analysis = pipelines_T54A.get_l2_analysis(subject_list, contrast_list, run_list, exp_dir, output_dir, working_dir, result_dir, data_dir)
                    
                    l2_analysis.run('MultiProc', plugin_args={'n_procs': 4})
                    
                if not os.path.isdir(opj(result_dir, output_dir, f"l3_analysis_equalIndifference_nsub_{n_sub}")) and 'l3' in operation:

                    contrast_list = ['ploss', 'pgain']
                    method = 'equalRange'

                    l3_analysis_equalRange = pipelines_T54A.get_group_workflow(subject_list, n_sub, contrast_list, 'equalRange',
                                                                  exp_dir, output_dir, working_dir, result_dir, data_dir)

                    l3_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 8})

                    contrast_list = ['ploss', 'pgain']
                    method = 'equalIndifference'

                    l3_analysis_equalIndiff = pipelines_T54A.get_group_workflow(subject_list, n_sub, contrast_list, 
                                                                                'equalIndifference', exp_dir, output_dir, 
                                                                                working_dir, result_dir, data_dir)

                    l3_analysis_equalIndiff.run('MultiProc', plugin_args={'n_procs': 8})

                    contrast_list = ['ploss']
                    method = 'groupComp'

                    l3_analysis_groupComp = pipelines_T54A.get_group_workflow(subject_list, n_sub, contrast_list, "groupComp", 
                                                                              exp_dir, output_dir, working_dir, 
                                                                              result_dir, data_dir)

                    l3_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_T54A.reorganize_results(result_dir, output_dir, n_sub, team_ID)


            if team_ID == "4TQ6":
                #FWHM to smooth
                fwhm = 6

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_run_id_01_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_4TQ6 = pipelines_4TQ6.get_l1_analysis(subject_list, run_list, TR, fwhm, exp_dir, 
                                             output_dir, working_dir, result_dir)

                    l1_analysis_4TQ6.run('MultiProc', plugin_args={'n_procs': 4})

                if not os.path.isdir(opj(result_dir, output_dir,"l2_analysis", f"_contrast_id_01_subject_id_{subject_list[-1]}")) and 'l2' in operation:
                    contrast_list = ['1', '2', '3', '4']

                    l2_analysis_4TQ6 = pipelines_4TQ6.get_l2_analysis(subject_list, contrast_list, run_list, exp_dir, 
                                             output_dir, working_dir, result_dir)

                    l2_analysis_4TQ6.run('MultiProc', plugin_args={'n_procs': 4})

                if not os.path.isdir(opj(result_dir, output_dir, f"l3_analysis_equalRange_nsub_{n_sub}")) and 'l3' in operation:
                    contrast_list = ['1', '2']

                    l3_analysis_equalRange = pipelines_4TQ6.get_group_workflow(subject_list, n_sub, contrast_list, "equalRange", exp_dir, 
                                             output_dir, working_dir, result_dir)

                    l3_analysis_equalRange.run('MultiProc')

                    l3_analysis_equalIndiff = pipelines_4TQ6.get_group_workflow(subject_list, n_sub, contrast_list, "equalIndifference", 
                                                                                exp_dir, output_dir, working_dir, result_dir)

                    l3_analysis_equalIndiff.run('MultiProc')

                    l3_analysis_groupComp = pipelines_4TQ6.get_group_workflow(subject_list, n_sub, contrast_list, "groupComp", exp_dir, 
                                                                             output_dir, working_dir, result_dir)

                    l3_analysis_groupComp.run('MultiProc')

                pipelines_4TQ6.reorganize_results(result_dir, output_dir, n_sub, team_ID)
                
            if team_ID == "X19V":
                #FWHM to smooth
                fwhm = 5

                if not os.path.isdir(opj(result_dir, output_dir,"l1_analysis", f"_run_id_01_subject_id_{subject_list[-1]}")) and 'l1' in operation:

                    l1_analysis_X19V = pipelines_X19V.get_l1_analysis(subject_list, run_list, TR, fwhm, exp_dir, 
                                             output_dir, working_dir, result_dir)

                    l1_analysis_X19V.run('MultiProc', plugin_args={'n_procs': 4})

                if not os.path.isdir(opj(result_dir, output_dir,"l2_analysis", f"_contrast_id_01_subject_id_{subject_list[-1]}")) and 'l2' in operation:
                    contrast_list = ['1', '2', '3', '4']

                    l2_analysis_X19V = pipelines_X19V.get_l2_analysis(subject_list, contrast_list, run_list, exp_dir, 
                                             output_dir, working_dir, result_dir)

                    l2_analysis_X19V.run('MultiProc',  plugin_args={'n_procs': 4})
                    
                if not os.path.isdir(opj(result_dir, output_dir, f"l3_analysis_groupComp_nsub_{len(subject_list)}")) and 'l3' in operation:
                    contrast_list = ['1', '2', '3', '4']
                    l3_analysis_equalRange = pipelines_X19V.get_group_workflow(subject_list, n_sub, contrast_list, "equalRange", exp_dir,
                                                                         output_dir, working_dir, result_dir, data_dir)

                    l3_analysis_equalRange.run('MultiProc',  plugin_args={'n_procs': 8})

                    l3_analysis_equalIndiff = pipelines_X19V.get_group_workflow(subject_list, n_sub, contrast_list, "equalIndifference",                                                                                                                        exp_dir, output_dir, working_dir, result_dir, data_dir)

                    l3_analysis_equalIndiff.run('MultiProc', plugin_args={'n_procs': 8})
                    l3_analysis_groupComp = pipelines_X19V.get_group_workflow(subject_list, n_sub, contrast_list, "groupComp", exp_dir,                                                                                                                                                                     output_dir, working_dir, result_dir, data_dir)
                    l3_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})

                pipelines_X19V.reorganize_results(result_dir, output_dir, n_sub, team_ID)
            if team_ID == "1KB2":
                fwhm = 7 
                if not os.path.isdir(opj(result_dir, output_dir,"preprocessing", f"_run_id_01_subject_id_{subject_list[-1]}")) and 'preprocess' in operation:
                    preprocess_1KB2 = pipelines_1KB2.get_preprocessing(exp_dir, result_dir, working_dir, output_dir, 
                                              subject_list, run_list, fwhm)
                    
                    preprocess_1KB2.run('MultiProc', plugin_args={'n_procs': 8})
               
                if not os.path.isdir(opj(result_dir, output_dir, 'l1_analysis', f"_run_id_01_subject_id_{subject_list[-1]}")) and 'l1' in operation:
                    l1_analysis_1KB2 = pipelines_1KB2.get_l1_analysis(subject_list, run_list, TR, exp_dir, output_dir, working_dir, result_dir)

                    l1_analysis_1KB2.run('MultiProc', plugin_args={'n_procs': 8})

                if not os.path.isdir(opj(result_dir, output_dir, 'l2_analysis', f"_contrast_id_2_subject_id_{subject_list[-1]}")) and 'l2' in operation:

                    contrast_list = ['1', '2']
                    l2_analysis_1KB2 = pipelines_1KB2.get_l2_analysis(subject_list, contrast_list, run_list, exp_dir, output_dir, working_dir, result_dir)

                    l2_analysis_1KB2.run('MultiProc', plugin_args={'n_procs': 8})
               
                if not os.path.isdir(opj(result_dir, output_dir, f"l3_analysis_groupComp_nsub_{len(subject_list)}")) and 'l3' in operation:
                    contrast_list = ['1', '2']
                    l3_analysis_equalRange = pipelines_1KB2.get_group_workflow(subject_list, n_sub, contrast_list, "equalRange", exp_dir,
                                                                         output_dir, working_dir, result_dir)

                    l3_analysis_equalRange.run('MultiProc', plugin_args={'n_procs': 8})

                    l3_analysis_equalIndiff = pipelines_1KB2.get_group_workflow(subject_list, n_sub, contrast_list, "equalIndifference",                                                                                                                        exp_dir, output_dir, working_dir, result_dir)

                    l3_analysis_equalIndiff.run('MultiProc', plugin_args={'n_procs': 8})
                    l3_analysis_groupComp = pipelines_1KB2.get_group_workflow(subject_list, n_sub, contrast_list, "groupComp", exp_dir,                                                                                                                                                                     output_dir, working_dir, result_dir)
                    l3_analysis_groupComp.run('MultiProc', plugin_args={'n_procs': 8})
                    
                pipelines_1KB2.reorganize_results(result_dir, output_dir, n_sub, team_ID)




































    








