# How to launch reproduction analyses? 

## Notebooks

To use the notebooks: 
- Download raw or fMRIprep datas available [**here**](https://openneuro.org/datasets/ds001734/versions/1.0.4) or in [**datalad**](http://datasets.datalad.org/?dir=/workshops/nih-2017/ds001734),
- Modify the paths:,
	- **exp_dir**: directory where the ds001734-download repository is stored (if you use the docker container, it will be "/data/ds001734-download"),
	- **result_dir**: directory where the intermediate and final repositories will be stored,
	- **working_dir**: name of the directory where intermediate results will be stored,
	- **output_dir**: name of the directory where final results will be stored,

- Select the number of subject you want to use by changing the value of the **nsub** variable.
	- For preprocessing & l1 analysis, all subjects will be analyzed. 
	- For l2 analysis, if **nsub < 108**, the required number of subject will be randomly selected from the dataset.

## Auto-analysis

Inside the `src` repository in the terminal, use: `python3 auto_analysis.py -t team_ID -s '["001", "002", "003", "004", "005", "006", "008", "009", "010", "011", "013", "014", "015", "016", "017", "018", "019", "020", "021", "022", "024", "025", "026", "027", "029", "030", "032", "033", "035", "036", "037", "038", "039", "040", "041", "043", "044", "045", "046", "047", "049", "050", "051", "052", "053", "054", "055", "056", "057", "058", "059", "060", "061", "062", "063", "064", "066", "067", "068", "069", "070", "071", "072", "073", "074", "075", "076", "077", "079", "080", "081", "082", "083", "084", "085", "087", "088", "089", "090", "092", "093", "094", "095", "096", "098", "099", "100", "102", "103", "104", "105", "106", "107", "108", "109", "110", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "123", "124"]' -e /data/ds001734-download -r /home/analytic_variability_fmri/data/derived/reproduced` 


The subject list can be modified, here we presented the full list but subjects can be removed and the analysis will be made only for the provided subjects. 

### Options 
#### `-t` or `--team_ID`

Teams for which to do the analysis, without '' or "". 
If you want to do the analysis for multiple teams, you have to add an other `-t + team_ID` option in the command line. 

#### `-s` or `--subjects`

Subjects for which to do the analysis. **Example**: '["001", "002"]'

#### `-e` or `--exp_dir`

Directory where data are stored. Typically, this will be `/data/ds001734-download` if you use the Docker container.

#### `-r` or `--result_dir`

Directory where the results will be stored. Typically, this will be `/home/analytic_variability_fmri/data/derived/reproduced` if you use the Docker container.

#### `-o` or `--operation`

Operations to perform during the analysis. One of "preprocess", "l1" or "l2". Multiple operations can be made.
If you want to perform multiple operations, you have to add an other `-o + the_operation` option in the command line. 
If you want to perform all possible operations for each team, just don't add this option. 