# ANALYTIC VARIABILITY IN FMRI STUDIES : Impact of sample size on the vibration of results

This project aims at analyzing the vibration of effects in results obtained when analyzing the same dataset with different methods. This vibration of effects will be studied for results obtained with growing sample sizes for groups. 

The objective is to show the evolution of vibration of effects with sample size and to determine if there is a sample size for which vibrations stabilize. 

We used the [**NARPS**](https://openneuro.org/datasets/ds001734/versions/1.0.4) dataset (Botzvinik-Nezer & al, 2020). 
We aim to reproduce 18 pipelines used by the teams participating in NARPS to study the vibration of results. 

## Table of contents
   * [Contents overview](#contents-overview)
   	  * [src](#src)
   	  * [data](#data)
   	  * [results](#results)
   	  * [figures](#figures)
   	  * [Other files](#other-files)
   * [Using the provided Docker container](#using-the-provided-docker-container)
   * [Reproducing full analysis](#reproducing-full-analysis)
      * [Download necessary data](#download-necessary-data)
      * [Use Notebooks](#use-notebooks)
   * [Reproducing figures](#reproducing-figures)
      * [Download necessary data](#download-necessary-data)
      * [Use Notebooks](#use-notebooks)
   * [References](#references)


## Contents overview 

### `src`

This directory contains scripts and notebooks used to reproduce pipelines used by the 18 teams selected from the NARPS study. 

It also contains the script used to determine the ID of the 18 teams to reproduce (see `select_pipelines.ipynb`). 

### `data`

This directory is made to contain data that will be used by scripts/notebooks stored in the `src` directory and to contain results of those scripts. 

### `results`

This directory contains notebooks and scripts that were used as quality control for the reproduction of pipelines. These notebooks were used to calculate comparison metrics between original statistical maps published by teams and reproduced ones. 
It also contains notebooks and scripts used to analyze analytical variability in the results obtained with the different pipelines with different sample sizes. 

### `figures`

This directory contains figures obtained when running the notebooks in the `results` directory. 

### Other files 

This repository contains two files : `instructions_create_docker_container.txt` and `Dockerfile` used to create the environment in which the notebooks could be launch. 

## Using the provided Docker container

Please follow instructions in the *instructions_create_docker_container.txt* file. 

If you don't want to use the Docker container, you can install [nipype](https://nipype.readthedocs.io/en/latest/users/install.html), [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation), [SPM](https://www.fil.ion.ucl.ac.uk/spm/software/download/) and [AFNI](https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/index.html) on your own computer. 

## Reproducing full analysis 

### Download necessary data 

Please follow instructions in the `data/README.md` file section "*NARPS Dataset*" [**HERE**](https://gitlab.inria.fr/egermani/analytic_variability_fmri/-/tree/master/data#narps-dataset).

### Use notebooks

To use notebooks, you need to adapt the paths for `exp_dir` and `results_dir` depending on your environment. 

If you use the docker container, you don't need to adapt these paths. 


## Reproducing figures

### Download necessary data

It requires derived data obtained by running the `src/reproduction_*.ipynb` notebooks. To avoid reproducing the full analysis, derived results can be downloaded from NeuroVault with a specific script. This script is executed in the `results/result_comparison_*.ipynb` notebook when data are not stored in the specific directory. 

It also requires original results provided by the teams. These data are also downloaded automatically when using the corresponding notebook. 

### Use notebooks

Figures can be obtained by running the `results/results_comparison_*.ipynb` notebooks. Each notebooks gives comparison results for one specific team. 

## References

Botvinik-Nezer, R., Holzmeister, F., Camerer, C.F., Dreber, A., Huber, J., Johannesson, M., Kirchler, M., Iwanir, R., Mumford, J.A., ..., Nichols, T.E., Poldrack, R.A., Schonberg, T. (2020). Variability in the analysis of a single neuroimaging dataset by many teams. Nature. https://doi.org/10.1038/s41586-020-2314-9.


